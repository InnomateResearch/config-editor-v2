// let searchWrapper = document.querySelector('.search-wrapper'),
//   searchInput = document.querySelector('.search-input');

// document.addEventListener('click', e => {
//   if (~e.target.className.indexOf('search')) {
//     searchWrapper.classList.add('focused');
//     searchInput.focus();
//   } else {
//     searchWrapper.classList.remove('focused');
//   }
// });

$(document).ready(function() {
  $('[data-toggle="tooltip"]').tooltip();
});

// options menu
// $(document).ready(function() {
//   $('#options').click(function(event) {
//     // event.stopPropagation();
//     console.log("on click options");
//     $('#dropdown').toggle();
//   });

//   $(document).click(function(event) {
//     var formContainer = $('#dropdown');
//     var btnLink = $('#options');
//     if (
//       formContainer.has(event.target).length === 0 &&
//       btnLink.has(event.target).length === 0
//     ) {
//       formContainer.hide();
//     }
//   });
// });

$(document).ready(function() {
  // $('#sysoptions').click(function(event) {
  //   // event.stopPropagation();
  //   //console.log("on click options");
  //   $('#sysdropdown').toggle();
  // });

  $(document).click(function(event) {
    var formContainer = $('#sysdropdown');
    var btnLink = $('#sysoptions');
    if (
      formContainer.has(event.target).length === 0 &&
      btnLink.has(event.target).length === 0
    ) {
      formContainer.hide();
    }
  });
});


// modal left menu
$(document).ready(function() {
  $('.submenu').hide();
  $('.modal-menu ul li a').click(function() {
    // .parent() selects the A tag, .next() selects the P tag
    $(this)
      .next()
      .slideToggle(200);
  });
  $('.submenu').slideUp(200);
});

// multi modal
$(document).ready(function() {
  $(document).on('show.bs.modal', '.modal', function(event) {
    var zIndex = 1040 + 10 * $('.modal:visible').length;
    $(this).css('z-index', zIndex);
    setTimeout(function() {
      $('.modal-backdrop')
        .not('.modal-stack')
        .css('z-index', zIndex - 1)
        .addClass('modal-stack');
    }, 0);
  });
});

/********************** 
  Drop Zone
**********************/

// $(document).ready(function() {
//   initFileUploader('#zdrop');
//   function initFileUploader(target) {
//     var previewNode = document.querySelector('#zdrop-template');
//     previewNode.id = '';
//     var previewTemplate = previewNode.parentNode.innerHTML;
//     previewNode.parentNode.removeChild(previewNode);

//     var zdrop = new Dropzone(target, {
//       url: 'upload.php',
//       maxFiles: 1,
//       maxFilesize: 30,
//       previewTemplate: previewTemplate,
//       previewsContainer: '#previews',
//       clickable: '#upload-label'
//     });

//     zdrop.on('addedfile', function(file) {
//       $('.preview-container').css('visibility', 'visible');
//     });

//     zdrop.on('totaluploadprogress', function(progress) {
//       var progr = document.querySelector('.progress .determinate');
//       if (progr === undefined || progr === null) return;

//       progr.style.width = progress + '%';
//     });

//     zdrop.on('dragenter', function() {
//       $('.fileuploader').addClass('active');
//     });

//     zdrop.on('dragleave', function() {
//       $('.fileuploader').removeClass('active');
//     });

//     zdrop.on('drop', function() {
//       $('.fileuploader').removeClass('active');
//     });
//   }
// });
