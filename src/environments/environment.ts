export const environment = {
  production: false,
  API :'https://configeditor.innomate.com.au',
  MAIN_SCHEMA : 'system-schema.json',
  SYSTEM_SCHEMA: 'system.json',
  ROOM_SCHEMA: 'room.json',
  SOFTWARE_PRJ : 'softwarejson',
  config: {
    tenant: '1da9d207-c9d2-4aba-8870-955b650b131f',
    //clientId: '14c39115-7cae-4c02-b865-20d7b2d205f8',
	clientId:'2a190ae8-718b-469d-8318-2a7c389c830e',
    endpoints: {
      'https://login.microsoftonline.com': '1da9d207-c9d2-4aba-8870-955b650b131f'

    }
  }
};