import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { InnomateModule } from './app/innomate.module';
import { environment } from './environments/environment';

if (environment.production) { enableProdMode(); }

platformBrowserDynamic().bootstrapModule(InnomateModule);
