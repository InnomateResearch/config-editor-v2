import { Component, OnInit, ViewChild, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
let saveAs = require('file-saver');
import { LoaderService } from '../loader.service';
import { AuthService } from '../services/auth.service';
import { Title } from '@angular/platform-browser';
import { SystemService } from '../system.service';
declare var $: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-system',
  templateUrl: 'system.component.html',
  animations: [
    trigger('expandSection', [
      state('in', style({ height: '*' })),
      transition(':enter', [
        style({ height: 0 }), animate(100),
      ]),
      transition(':leave', [
        style({ height: '*' }),
        animate(100, style({ height: 0 })),
      ]),
    ]),
  ],
  styleUrls: ["./system.component.scss"]
})
export class SystemComponent implements OnInit {
  // @ViewChild("dialogRef") dialogRef: any;
  isLoading: any;
  @Output() reloadEvent = new EventEmitter<string>();
  @Input() selectedSystem: any;
  showRoomSchema: boolean = false;
  isSaved: boolean = true;
  sideNavobj = new Array();
  forTheFirstTime: boolean = false;
  systemData: any;
  public userName: string;
  deployMsg: any;
  forSideNav: any;
  files: File[] = [];
  previous: any;
  openNewSideDiv: boolean = false;
  forOpenDivBoolean: boolean = false;
  repoValue: string;
  repos: any[] = [];
  version: string;
  count = 0;
  nameCheck = false;
  errorMsg: any[] = [];
  operation = '';
  deploySetName = '';
  //CSS frame works
  selectedSetName = '';
  selectedFramework = 'bootstrap-4';
  selectedRepo = '';
  duplicateSystem = '';
  visible = {
    options: true,
    schema: true,
    form: true,
    output: true
  };

  formActive = false;
  jsonFormValid = false;
  jsonFormStatusMessage = 'Loading form...';
  jsonFormSchema: any;
  isShowError: string;

  //json schema form options
  jsonFormOptions: any = {
    addSubmit: false, // Add a submit button if layout does not have one
    //addTitle: 'Save' ,
    debug: false, // Don't show inline debugging information
    loadExternalAssets: true, // Load external css and JavaScript for frameworks
    returnEmptyFields: false, // Don't return values for empty input fields
    fieldsRequired: true,
    setSchemaDefaults: true, // Always use schema defaults for empty fields
    defautWidgetOptions: { feedback: true }, // Show inline feedback icons
  };

  showErrors: boolean = false;
  formIsValid = null;
  keysLength = 0;
  selectedSideNavItem: any;
  schemaKeysList: any;
  roomSchemaKeysList: any;
  schemaKeyValue = new Array();
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router, private dialog: MatDialog,
    public loaderService: LoaderService,
    private authService: AuthService,
    private titleService: Title,
    private systemService: SystemService,
    private cdr: ChangeDetectorRef
  ) {
    this.userName = this.authService.getUserName();
    this.titleService.setTitle("Innomate System Configuration");
  }
  logout(): void {
    this.router.navigate(['logout']);
  }
  reloadSystems(callBackEvent) {
    console.log("callBackEvent=", callBackEvent)
    this.showErrors = false;
    this.reloadEvent.next(callBackEvent);
  }
  ngOnInit() {
    this.forTheFirstTime = true;
    // Subscribe to query string to detect schema to load
    this.route.queryParams.subscribe(
      params => {
        this.operation = params.operation;
        this.selectedRepo = params.page;
        this.selectedSystem = params.page;
        if (this.selectedRepo == '' || this.selectedRepo == undefined) {
          this.nameCheck = true;
        }
        else {
          this.nameCheck = false;
        }
      }
    );
  }
  
  onSelect(event: any) {
    console.log("slect files ");
    this.files = [];
    this.files.push(...event.addedFiles);
  }
  onLoadSysFile(){
    this.files = [];
  }
  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }
  onDeploy() {
    this.showMessage("Successfully saved..!", true, 3000);
  }
  showMessage(msg, isError, ms) {
    var x = document.getElementById("syssnackbar");
    this.isShowError = msg;
    x.className = "show";
    if (isError) {
      $("#syssnackbar").css('background-color', "#f44259").css('color', "#fff");
    }
    else
      $("#syssnackbar").css('background-color', "#07ddc3").css('color', "#000");;
    setTimeout(function () { this.isShowError = ""; }, ms);
  }

  duplicateConfig() {
    this.duplicateSystem = this.selectedRepo;
    this.operation = 'create';
    this.selectedRepo = '';
    if (this.jsonFormSchema) {
      this.jsonFormSchema.properties.Properties.properties["System Name"].readonly = false;
      this.jsonFormSchema.properties.Properties.properties["System Repository Name"].readonly = false;
      this.isSaved = false;
      let schema = JSON.stringify(this.jsonFormSchema);
      let system = JSON.stringify(this.systemData);
      this.generateForm(schema, system);
    }
    this.nameCheck = true;
    this.showMessage(
      'The configuration for ['+this.duplicateSystem+'] has been successfully duplicated., Please "save" it.',
      false,
      3000
    );
  }
  checkSpecialChar(str) {
    var format = /[!@#$%^&*()_+\=\[\]{};':"\\|,.<>\/?]+/;

    if (format.test(str)) {
      return true;
    } else {
      return false;
    }
  }
  hasLowerCase(str) {
    return (/[A-Z]/.test(str));
  }

  //form submit data
  onSubmit(data: any) {

    let file = new Blob([JSON.stringify(data)], { type: 'application/json;charset=utf-8' });
    saveAs(file, `system.json`);
  }

  //discard changes
  discardChanges() {
    this.router.navigateByUrl('');
  }

  //download file
  onClickDownloadFile(event: any, data: any) {

    let file = new Blob([JSON.stringify(data)], { type: 'application/json;charset=utf-8' });
    saveAs(file, `system.json`);
  }

  isValid(isValid: boolean): void {
    this.formIsValid = isValid;
  }

  validationErrors(data: any): void {
    this.errorMsg = [];
    if (data) {
      data.forEach(element => {
        let msg = "";
        let propertyName = "";
        let errAt = element["dataPath"]
          .toString()
          .slice(1)
          .replace(/\//g, ">>");
          var requiredFields = this.jsonFormSchema.properties[errAt.split(">>")[0]].required;
          if(requiredFields != undefined){
            if(errAt.split(">>")[1] == undefined || requiredFields.includes(errAt.split(">>")[1])){
              if (element["keyword"] == "required") {
                propertyName = element.params.missingProperty;
                msg = "<b>" + propertyName + "</b> is required";
              }
              else {
                if (element["message"] != undefined) {
                  msg = element["message"];
                  msg = msg.replace("'", "<b>");
                  msg = msg.replace("'", "</b>");
                }
              }
      
              this.errorMsg.push({ at: errAt, msg: msg });
            }
           
          }
      
      });
    }
    else {
      this.showErrors = false;
      $(".deploy-css").hide();
      $(".error-css").hide();
    }
  }

  //In your component:
  uploadFile(event, schema) {
    if (this.files.length !== 1) {
      console.error('No file selected');
    } else {
      const reader = new FileReader();
      reader.onloadend = (e) => {
        this.isLoading = true;
        this.isSaved = false;

        this.generateForm(JSON.stringify(schema), reader.result.toString());
        this.showMessage("New configuration has loaded. Please 'Save' it, to persist.", false, 3000);
      };
      reader.readAsText(this.files[0]);
    }
    $('#uploadsystemFile').modal('hide');
  }

  //save the System
  onSave(type: any) {

    let title = this.systemData.Properties["System Repository Name"];
    let systemName = this.systemData.Properties["System Name"];
    this.version = this.systemData.Properties["System Version"];
    this.deploySetName = systemName;

    if (this.errorMsg.length > 0) {
      $('.error-css').show();
      this.showErrors = true;
      $("#formErrors").modal("show");
      return;
    }
    else {
      if (type == 'deploy') {
        return;
      }
      this.showErrors = false;
    }
    if (title.includes(' ') || this.hasLowerCase(title) || this.checkSpecialChar(title)) {
      this.errorMsg.push({ at: 'Properties', msg: 'Entered System Repository Name should not contain spaces/uppercase/special characters' });
      $('.error-css').show();
      this.showErrors = true;
      this.isLoading = false;
      $("#formErrors").modal("show");
      return;
    }

    let isRoomSchemaChecked = false;
    this.schemaKeysList.forEach((k: any, i: any) => {
      if (this.schemaKeyValue[k]) {
        isRoomSchemaChecked = true;
      }
    });
    if (!isRoomSchemaChecked) {
      $("#roomSchemaWarn").modal("show");
      return;
    }
    this.isLoading = true;
    if (this.selectedRepo == '' || this.selectedRepo == undefined) {
      this.systemService.repositoryList().subscribe(schema => {
        let systemRepoNames = JSON.parse(JSON.stringify(schema));
        for (let element in systemRepoNames) {
          let key = systemRepoNames[element].repository;
          let value = systemRepoNames[element].system_name;
          if (title == key) {
            this.errorMsg.push({ at: 'Properties', msg: 'Entered System Repository Name Already Exists' });
            $('.error-css').show();
            this.showErrors = true;
            this.isLoading = false;
            return;
          }
          if (systemName == value) {
            this.errorMsg.push({ at: 'Properties', msg: 'Entered System Name Already Exists' });
            $('.error-css').show();
            this.showErrors = true;
            this.isLoading = false;
            return;
          }
        }
        console.log("exit")
        this.systemService.getMainSchema().subscribe(schema => {
          let sma = JSON.stringify(schema);

          this.systemService.createSystem(title, this.systemData.Properties["System Name"])
            .subscribe(schema => {
              this.systemService.commitfile1(title, 'system.json', JSON.stringify(this.systemData)).subscribe(data => {
                this.systemService.commitfile2(title, 'system-schema.json', sma).subscribe(data => {
                  this.systemService.getMainRoomSchema().subscribe(rmschema => {
                    let rmschma = JSON.stringify(rmschema);
                     //Save room-schema
                  this.systemService.commitfile2(title, 'room-schema.json', JSON.stringify(this.getRoomMainSchema(rmschma))).subscribe(data => {
                    this.cdr.detectChanges();
                  },
                    error => {
                      //this.isLoading = false;
                      this.deployMsg = error.error;
                      $('.deploy-css').show();
                      this.showMessage(error.error, true, 3000);
                    });
                  // end Save room-schema
                  },
                    error => {
                      this.isLoading = false;
                      this.deployMsg = error.error;
                      $('.deploy-css').show();
                      this.showMessage(error.error, true, 3000);
                    });
                 
                  this.isSaved = false;
                  if (type == 'save') {
                    this.showMessage('"' + systemName + '" created successfully!', false, 3000);
                  }
                  this.systemService.updatepipeLine(title)
                    .subscribe(response => {
                      this.showMessage("Updated repo variables successfully!", false, 3000);
                      this.systemService.pipeLine(title)
                        .subscribe(response => {
                          this.showMessage('Pipeline executed successfully!', false, 3000);
                          if (type == 'exit') {
                            this.isLoading = false;
                            this.router.navigate([''], { queryParams: { repo: systemName, 'operation': 'create' } });
                          } else {
                            this.isLoading = false;
                            this.operation = 'edit';
                            this.loadSelectedSystem(title);
                            //this.router.navigate([''], { queryParams: { page: title, 'name': title, 'operation': 'edit' } });
                          }
                        },
                          error => {
                            this.isLoading = false;
                            this.deployMsg = error.error;
                            $('.deploy-css').show();
                            this.showMessage(error.error, true, 3000);
                          });
                    },
                      error => {
                        this.isLoading = false;
                        this.deployMsg = error.error;
                        $('.deploy-css').show();
                        this.showMessage(error.error, true, 3000);
                      });
                },
                  error => {
                    this.isLoading = false;
                    this.deployMsg = error.error;
                    $('.deploy-css').show();
                    this.showMessage(error.error, true, 3000);
                  });
              },
                error => {
                  this.isLoading = false;
                  this.deployMsg = error.error;
                  $('.deploy-css').show();
                  this.showMessage(error.error, true, 3000);
                });
            },
              error => {
                this.isLoading = false;
                this.deployMsg = error.error;
                $('.deploy-css').show();
                this.showMessage(error.error, true, 3000);
              });
        },
          error => {
            this.isLoading = false;
            this.deployMsg = error.error;
            $('.deploy-css').show();
            this.showMessage(error.error, true, 3000);
          });

      },
        error => {
          this.isLoading = false;
          this.deployMsg = error.error;
          $('.deploy-css').show();
          this.showMessage(error.error, true, 3000);
        });
    } else {

      this.systemService.getcommitFile(this.selectedRepo, 'system.json', JSON.stringify(this.systemData)).subscribe(data => {
        this.isSaved = false;
        //Save room-schema
        this.systemService.getMainRoomSchema().subscribe(rmschema => {
          let rmschma = JSON.stringify(rmschema);
           //Save room-schema
           this.systemService.getcommitFile(this.selectedRepo, 'room-schema.json', JSON.stringify(this.getRoomMainSchema(rmschma))).subscribe(data => { },
            error => {
              // this.isLoading = false;
              this.deployMsg = error.error;
              $('.deploy-css').show();
              this.showMessage(error.error, true, 3000);
            });
          // end Save room-schema
        },
          error => {
            this.isLoading = false;
            this.deployMsg = error.error;
            $('.deploy-css').show();
            this.showMessage(error.error, true, 3000);
          });
        this.systemService.updatepipeLine(title)
          .subscribe(response => {
            this.showMessage("Updated repo variables successfully!", false, 3000);
            this.systemService.editpipeLine(title)
              .subscribe(response => {
                this.showMessage('Pipeline executed successfully!', false, 3000);
                this.showMessage('"' + systemName + '" saved successfully!', false, 3000);
                if (type == 'exit') {
                  this.isLoading = false;
                  this.router.navigate([''], { queryParams: { repo: systemName, 'operation': 'edit' } });
                } else {
                  this.isLoading = false;
                  //this.router.navigate([''], { queryParams: { page: this.selectedRepo, 'name': this.selectedRepo, 'operation': 'edit' } });
                  this.operation = 'edit';
                  this.loadSelectedSystem(this.selectedRepo);
                }
              });
          });

      },
        error => {
          this.isLoading = false;
          this.deployMsg = error.error;
          $('.deploy-css').show();
          this.showMessage(error.error, true, 3000);
        });
    }
  }

  private getRoomSchema() {
    let required = [];
    this.schemaKeysList.forEach((k: any, i: any) => {
      if (this.schemaKeyValue[k]) {
        required.push(k);
      }
    });
    console.log("required=", required);
    let roomSchema = this.jsonFormSchema;
    roomSchema["required"] = required;
    this.schemaKeysList.forEach((k: any, i: any) => {
      if (!this.schemaKeyValue[k]) {
        delete roomSchema.properties[k];
      }
    });

    return roomSchema;
  }

  private getRoomMainSchema(roomSchma : string) {
    let required = [];
    this.schemaKeysList.forEach((k: any, i: any) => {
      if (this.schemaKeyValue[k]) {
        required.push(k);
      }
    });
    console.log("required=", required);
    let roomSchema = JSON.parse(roomSchma);
    roomSchema["required"] = required;
    this.schemaKeysList.forEach((k: any, i: any) => {
      if (!this.schemaKeyValue[k]) {
        delete roomSchema.properties[k];
      }
    });

    return roomSchema;
  }

  deploy() {

    let title = this.systemData.Properties["System Repository Name"];
    let systemName = this.systemData.Properties["System Name"];
    this.isLoading = true;
    $('#mySystemModal').modal('hide');

    this.systemService.systemdeploy(title, systemName).subscribe(data => {
      this.reloadSystems("deploy");
      this.isLoading = false;
      $('#editSystem').modal('hide');
    },
      error => {
        this.isLoading = false;
        this.deployMsg = error.error;
        $('.deploy-css').show();
        this.showMessage(error.error, true, 3000);
      });

  }

  close() {
    this.reloadSystems("close");
  }

  //revertChanges

  revertChanges() {

    this.isSaved = false;
    this.loadSelectedSystem(this.selectedRepo)
    this.showMessage('Reverted changes successfully!', false, 3000);
  }

  deleteSystem(system_name: any, repository: any) {
    this.systemService.deleteSysRooms(this.selectedSystem, this.selectedSystem).subscribe(data1 => {
    this.systemService.deleteSystem(repository, repository).subscribe(data => {
      $('#deleteSystem').modal('hide');
      this.reloadSystems("delete");
      this.systemService.deleteSystemVC4().subscribe(info =>{
        
      })
    })
  });
  }
  //On load
  loadSelectedSystem(selectedRepo) {
    this.openNewSideDiv = false;
    this.showRoomSchema = false;
    this.schemaKeyValue = [];
    this.roomSchemaKeysList = [];
    this.formActive = false;
    this.isShowError = "";
    this.jsonFormSchema = {};
    this.sideNavobj = [];
    this.selectedRepo = selectedRepo;
    this.isLoading = true;
    $(".deploy-css").hide();
    $(".error-css").hide();
    $('#sysdropdown').hide();
    if (this.operation == "create") {
      this.systemService.getMainSchema1().subscribe(schema => {
        this.generateForm(JSON.stringify(schema), null);
      },
        error => {
          this.isLoading = false;
          this.deployMsg = error.error;
          $('.deploy-css').show();
          this.showMessage(error.error, true, 3000);
        });
    } else {

      this.systemService.getSystemSchema(this.selectedRepo).subscribe(schema => {

        this.systemService.filterSystem(this.selectedRepo).subscribe(data => {
          this.systemService.filterRoomSchema(this.selectedRepo).subscribe(roomSchema => {
            let schemaObj = JSON.parse(JSON.stringify(roomSchema));
            this.roomSchemaKeysList = Object.keys(schemaObj.properties);
            let jsonData = JSON.parse(JSON.stringify(data));
            this.version = jsonData.Properties["System Version"];
            this.selectedSetName = jsonData.Properties["System Name"];
            this.generateForm(JSON.stringify(schema), JSON.stringify(data));
          },
            error => {
              this.isLoading = false;
              this.deployMsg = error.error;
              $('.deploy-css').show();
              this.showMessage(error.error, true, 3000);
            });
        },
          error => {
            this.isLoading = false;
            this.deployMsg = error.error;
            $('.deploy-css').show();
            this.showMessage(error.error, true, 3000);
          });

      },
        error => {
          this.isLoading = false;
          this.deployMsg = error.error;
          $('.deploy-css').show();
          this.showMessage(error.error, true, 3000);
        });
    }
    this.forTheFirstTime = false;
  }

  removeActiveClasses() {
    $("a.active").removeClass("active");
    setTimeout(() => {
      $(".heading-0").addClass("active");
      $('#inner-' + 0).css('display', 'block');
    }, 0);
  }

  showLoading() {
    this.loaderService.showLoading();
  }

  hideLoading() {
    this.loaderService.hideLoading();
  }
  // Display the form entered by the user
  // (runs whenever the user changes the jsonform object)
  generateForm(schema: string, system: string) {
    this.jsonFormSchema = {};
    this.systemData = {};

    if (!schema) {
      console.log("schema is empty");
      return;
    }
    this.jsonFormStatusMessage = 'Loading form...';
    this.formActive = false;

    try {

      // Parse entered content as JSON
      this.jsonFormSchema = JSON.parse(schema);

      if (this.selectedRepo != "") {
        this.jsonFormSchema.properties.Properties.properties["System Name"].readonly = true;
        this.jsonFormSchema.properties.Properties.properties["System Repository Name"].readonly = true;
      }
      if (system) {
        this.systemData = JSON.parse(system);
      }
      this.roomSchemaKeysList.forEach((k: any, i: any) => {
        this.schemaKeyValue[k] = true;
      });
      this.schemaKeyValue["Properties"] = true;
      this.schemaKeysList = Object.keys(this.jsonFormSchema.properties);
      this.keysLength = this.schemaKeysList.length;
      this.schemaKeysList.forEach((k: any, i: any) => {
        if (i == 0) {
          this.jsonFormSchema.properties[k]["htmlClass"] = "cust-label-class" + i;
        }
        else {
          this.jsonFormSchema.properties[k]["htmlClass"] = "cust-label-class" + i + " prop-hide";
        }
      });
      this.jsonFormValid = true;
      this.generateSideNav();
      this.formActive = true;
      this.isLoading = false;
      this.loadFileTree();
    } catch (jsonError) {
      this.isLoading = false;
      /*  setTimeout(() => {
         this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
         this.router.navigate([""]))
     }, 5000);  //5s
     */
      this.formActive = false;
      this.jsonFormValid = false;
      this.jsonFormStatusMessage =
        'Entered content is not currently a valid JSON Form object.\n' +
        'As soon as it is, you will see your form here. So keep typing. :-)\n\n' +
        'JavaScript parser returned:\n\n' + jsonError;
      this.showMessage(jsonError, true, 3000);
      return;
    }

    // setTimeout(() => {
    //   this.Jqhide();
    // }, 100);
  }

  toggleVisible(item: string) {
    this.visible[item] = !this.visible[item];
  }

  toggleFormOption(option: string) {
    if (option === 'feedback') {
      this.jsonFormOptions.defautWidgetOptions.feedback =
        !this.jsonFormOptions.defautWidgetOptions.feedback;
    } else {
      this.jsonFormOptions[option] = !this.jsonFormOptions[option];
    }

    this.generateForm(JSON.stringify(this.jsonFormSchema), JSON.stringify(this.systemData));
  }


  onCreateSideNav() {
    let sideNav = [];
    let side = "n";
    let bottom = "n";
    let pro = this.jsonFormSchema.properties;
    Object.keys(pro).forEach(function (k) {
      if (pro[k].type == "object") {
        let finalItem = [];
        let item = Object.keys(pro[k].properties);

        for (let i = 0; i < item.length; i++) {
          if (pro[k].properties[item[i]].type == "array" || pro[k].properties[item[i]].type == "object") {
            bottom = "y";
            finalItem.push({ key: item[i], value: "" });
          }
        }
        sideNav.push({ key: k, items: finalItem, side: side, bottom: bottom });

      }
      if (pro[k].type == "array") {
        sideNav.push({ key: k, items: [], side: side, bottom: bottom });
      }

    });
    return sideNav;
  }
  loadFileTree() {
    $(document).ready(function () {
      $(".pull-right").each(function () {
        if ($(this).find("span").html()) {
          if ($(this).find("span").html().includes("Add to ")) {
            $(this).find("span").text(function (index, text) {
              return text.replace('Add to', 'Add');
            });
          }
        }
      });

      $('.submenu').hide();
      $('.modal-menu ul li a.m-head').click(function () {
        $('.submenu').slideUp(200);
        $(this).next().slideToggle(200);
      });
      $('.submenu').slideUp(200);
    });
  }
  generateSideNav() {

    if (this.operation == "edit") {
      let systemSideNavObj = this.systemData;
      let schemaSideNavObj = this.jsonFormSchema.properties;
      //console.log("schemaSideNavObj=",schemaSideNavObj)
      let arr = new Array();
      Object.keys(schemaSideNavObj).forEach(function (k) {
        let side = "n";
        let bottom = "n";
        let item = [];
        let finalItem = [];
        let keysArr = [];

        if (Array.isArray(systemSideNavObj[k])) {
          side = "y";
          bottom = "n";
          let data = [];
          data = Object["values"](systemSideNavObj[k]);
          data = Object["values"](systemSideNavObj[k]);
          for (let j = 0; j < data.length; j++) {
            if (data[j]["ID"])
              keysArr.push(data[j]["ID"]);
            else if (data[j]["Name"])
              keysArr.push(data[j]["Name"]);
          }
          item = keysArr;
        } else {
          side = "n";
          bottom = "n";
          if (systemSideNavObj[k]) {
            {
              let scmItems = [];
              finalItem = [];
              item = Object.keys(systemSideNavObj[k]);
              if (Array.isArray(schemaSideNavObj[k].properties))
                scmItems = schemaSideNavObj[k].properties;
              if (typeof schemaSideNavObj[k].properties === 'object')
                scmItems = Object.keys(schemaSideNavObj[k].properties);                
              for (let i = 0; i < item.length; i++) {
                if (Array.isArray(systemSideNavObj[k][item[i]]) || typeof systemSideNavObj[k][item[i]] === 'object') {
                  bottom = "y";
                }
              }

              if (bottom == "y") {
                for (let i = 0; i < scmItems.length; i++) {
                  finalItem.push({ key: scmItems[i] });
                }
                item = finalItem;
              }
            }
          }
          else{
            bottom = "y";
            let scmItems = [];
            if (typeof schemaSideNavObj[k].properties === 'object')
                scmItems = Object.keys(schemaSideNavObj[k].properties);
            for (let i = 0; i < scmItems.length; i++) {
              finalItem.push({ key: scmItems[i] });
            }
            item = finalItem;
          }
        }
        if (item.length == 0) {
          side = "n";
        }
        arr.push({ key: k, items: item, side: side, bottom: bottom });
      });
      this.sideNavobj = arr;
    } else {
      this.sideNavobj = this.onCreateSideNav();
      setTimeout(() => {

        for (let i = 0; i < this.sideNavobj.length; i++) {
          $('#inner-' + i).css('display', 'none');
        }
        $('#inner-' + 0).css('display', 'block');
      }, 0);
    }
    setTimeout(() => {
      $("a.active").removeClass("active");
      $(".heading-0").addClass("active");
    }, 0);
  }

  onMenuItemRoomSchema() {
    this.openNewSideDiv = false;
    this.showRoomSchema = true;
    console.log("showRoomSchema=", this.showRoomSchema)
    $("a.active").removeClass("active");
    $(".heading-" + this.keysLength).addClass("active");
    for (let i = 0; i < this.keysLength; i++) {
      $(".cust-label-class" + i).addClass("prop-hide");
    }
  }

  onMenuItemClicked(heading, index) {
    //console.log("onMenuItemClicked=", heading);
    //console.log("index=", index);
    this.showRoomSchema = false;
    this.selectedSideNavItem = heading.key;
    // this.forSideNav = heading;
    let systemSideNavObj = this.systemData;
    let data = [];
    let keysArr = [];

    Object.keys(systemSideNavObj).forEach(function (k) {
      if (k === heading.key) {
        data = Object["values"](systemSideNavObj[k]);
        for (let j = 0; j < data.length; j++) {
          if (data[j]["ID"])
            keysArr.push(data[j]["ID"]);
          else if (data[j]["Name"])
            keysArr.push(data[j]["Name"]);
        }
      }
    });
    this.forSideNav = { key: heading.key, items: keysArr, side: heading.side, bottom: heading.bottom };

    $("a.active").removeClass("active");
    $(".heading-" + index).addClass("active");

    if (heading.side == "y") {
      this.openNewSideDiv = true;
    }
    else {
      this.openNewSideDiv = false;
    }
    this.cdr.detectChanges();
    for (let i = 0; i < this.keysLength; i++) {
      $(".cust-label-class" + i).addClass("prop-hide");      
    }
    //console.log("heading.side=",heading.side, "heading.bottom=", heading.bottom ,"index=", index,"this.keysLength=",this.keysLength);
    $(".cust-label-class" + index).removeClass("prop-hide");

    if (heading.side == "n" && heading.bottom == "n") {
      let legendFlag = true;
      $(".cust-label-class" + index).find("legend").each(function () {
        if (heading.key == $(this).text()) {
          legendFlag = false;
          $(this).parent().find('input:first').focus();
        }
      });
      if (legendFlag) {
        $(".cust-label-class" + index).find("label").each(function () {
          if (heading.key == $(this).text()) {
            $(this).parent().find('input:first').focus();
          }
        });
      }
    }   
  }

  onBottomMenuItemClicked(heading, index) {
    // console.log("bottom itemheading=", heading, " index=", index);
    let systemSideNavObj = this.systemData;
    let side = "n";
    let bottom = "n";

    let data = [];
    let keysArr = [];
    this.openNewSideDiv = true;
    Object.keys(systemSideNavObj).forEach(function (k) {
      if (k === heading.key) {
        data = Object["values"](systemSideNavObj[k]);
        if (data[index]) {
          for (let j = 0; j < data[index].length; j++) {

            if (data[index][j]["ID"])
              keysArr.push(data[index][j]["ID"]);
            else if (data[index][j]["Name"])
              keysArr.push(data[index][j]["Name"]);
          }
        }
      }
    });


    let headingIndex = 0;
    this.selectedSideNavItem = heading.key;
    if (keysArr.length != 0) {
      this.openNewSideDiv = true;
    }
    else {
      this.openNewSideDiv = false;
    }
    this.forSideNav = { key: heading.key, items: keysArr, side: side, bottom: bottom, selected: heading.items[index].key };

    $("a.active").removeClass("active");
    for (let i = 0; i < this.keysLength; i++) {
      $(".cust-label-class" + i).addClass("prop-hide");
    }
    this.schemaKeysList.forEach((k: any, i: any) => {
      if (k === heading.key) {
        headingIndex = i;
        $(".cust-label-class" + i).removeClass("prop-hide");
        $(".heading-" + i).addClass("active");
      }
    });

    $(".sub-heading-" + index).addClass("active");
    if (keysArr.length == 0) {
      let legendFlag = true;
      $(".cust-label-class" + headingIndex).find("legend").each(function () {
        if (heading.items[index].key == $(this).text()) {
          legendFlag = false;
          $(this).parent().find('input[type=text]:first').focus();

        }
      });
      if (legendFlag) {
        $(".cust-label-class" + headingIndex).find("label").each(function () {
          if (heading.items[index].key == $(this).text()) {
            $(this).parent().find('input[type=text]:first').focus();

          }
        });
      }


    }
    else {

    }


  }

  onSideMenuItemClicked(heading, subHeading, index) {
    //   console.log("heading=", heading, index);
    let headingIndex = 0;
    this.schemaKeysList.forEach((k: any, i: any) => {
      if (k === heading.key) {
        headingIndex = i;
      }
    });
    $(".cust-label-class" + headingIndex).find("legend").each(function (i) {
      if (!heading.selected) {
        $(this).parent().find('input').each(function () {
          if (subHeading == $(this).val()) {
            $(this).focus();
            return false;
          }
        });;
      }
      else if (heading.selected == $(this).text()) {
        $(this).parent().find('input[type=text]').each(function () {
          if (subHeading == $(this).val()) {
            $(this).focus();
            return false;
          }
        });;
      }
    });
    heading.items.forEach((k: any, i: any) => {
      $(".sidenav-heading-" + i).removeClass("active");
    });
    $(".sidenav-heading-" + index).addClass("active");
  }
  hideAll() {
    //paragraph
    $("fieldset legend:first").hide();
    $("fieldset legend:first").next("div").hide();
    //$("bootstrap-3-framework div p").hide();

    $(".schema-form-section div p:first").hide();

    $(".schema-form-array div p:first").hide();
    $('bootstrap-3-framework').find('.schema-form-section div:first p:first').hide();
    $('bootstrap-3-framework').find('.schema-form-array div:first p:first').hide();

    //main heading
    $("fieldset").find("legend:first").hide();
    $("fieldset").find("legend:first").next("div").hide();

    //Actual panel
    $("section-widget root-widget").hide();
  }
  addCss() {
    //$("section-widget label[class^='ng-star-inserted']:first").addClass("lbl-header");
    $("section-widget label[class^='ng-star-inserted']:first-child").addClass("lbl-header");
    //$(".schema-form-array label:first").addClass("lbl-header");
    $(".schema-form-array").find("label:first").addClass("lbl-header");

    $('bootstrap-3-framework').find('.schema-form-section').addClass('form-group-header');
    $('bootstrap-3-framework').find('.schema-form-array').addClass('form-group-header');
    $('bootstrap-3-framework').find('button').parent().addClass('form-group-header-box');
    // console.log($('bootstrap-3-framework').find('button').parent().closest("add-reference-widget").parent().closest("bootstrap-3-framework").html());
    $('bootstrap-3-framework').find('button').parent().closest("add-reference-widget").parent().closest("bootstrap-3-framework").find("div:first").addClass('schema-form-ref');
  }

  handbeger() {
    $(".hamburger").click(function () {
      $(this).find(".dropdown-menu").slideToggle("fast");
    });
    $(document).on("click", function (event) {
      var $trigger = $(".hamburger");
      if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".dropdown-menu").slideUp("fast");

      }
    });

    $(".header__menu").click(function (e) {
      e.stopPropagation(); // This is the preferred method.
      return false;        // This should not be used unless you do not want
      // any click events registering inside the div
    });
  }

  Jqhide() {
    $(".deploy-css").hide();

    this.hideAll();
    this.addCss();

    $("section-widget label[class^='ng-star-inserted']:first-child").bind(
      "click",
      function (e) {
        $(this).toggleClass("active-acc");
        $(this)
          .parent()
          .find("root-widget")
          .fadeToggle();
        $(this)
          .parent()
          .find("root-widget input:first")
          .focus();

        $(this)
          .parent()
          .find("root-widget label.ng-star-inserted")
          .removeClass("lbl-header")
          .unbind();
        $(this)
          .parent()
          .find("section-widget label.ng-star-inserted")
          .removeClass("lbl-header")
          .unbind();
      }
    );

    $(".schema-form-array")
      .find("label:first")
      .bind("click", function (e) {
        $(this).toggleClass("active-acc");
        $(this)
          .parent()
          .find("root-widget")
          .fadeToggle();
        $(this)
          .parent()
          .find("root-widget input:first")
          .focus();
        $(this)
          .parent()
          .find("section-widget label.ng-star-inserted")
          .removeClass("lbl-header")
          .unbind();
      });

    $(".file-tree").filetree();
    $(".file-list .folder-root").on("click", function () {
      if ($(this).hasClass("closed")) {
        $(this).removeClass("closed");
        $(this).addClass("open");
      } else {
        $(this).removeClass("open");
        $(this).addClass("closed");
      }
      return false;
    });

    if (this.isSaved) {
      this.handbeger();
    }
  }

  openAccordian(selectedSideHeading) {
    if (selectedSideHeading) {
      $("section-widget label:contains('" + selectedSideHeading + "')").click();
      $(".schema-form-array label:contains('" + selectedSideHeading + "')").click();
      // $(".schema-form-array label:contains('"+selectedSideHeading+"')").css('background-color', 'red');
    }
  }
  onClickSideNavSub(selectedSideHeading, subHeading, index) {
    let isFormArray = false;
    if (selectedSideHeading) {
      var cssName = $("section-widget label:contains('" + selectedSideHeading + "')").attr('class');
      if (!cssName) {
        isFormArray = true;
        cssName = $(".schema-form-array label:contains('" + selectedSideHeading + "')").attr('class');
      }
      if (subHeading) {
        if (isFormArray && Number.isInteger(index)) {
          var dv = $(".schema-form-array label").filter(function () {
            return $(this).text() == selectedSideHeading;
          }).siblings().find("label").filter(function () {
            return $(this).text().toLowerCase() == subHeading.toLowerCase();
          });
          dv.each(function (el, el2) {
            if (index == el) {
              this.click();
            }
          });
        }
        else {
          $("section-widget label:contains('" + subHeading + "')").click();
        }
      }
    }
  }
  openAccordianSub(selectedSideHeading, subHeading, index) {
    //console.log("selectedSideHeading=" + selectedSideHeading + " subHeading=" + subHeading + " index=" + index);

    if (selectedSideHeading) {
      var cssName = $("section-widget label:contains('" + selectedSideHeading + "')").attr('class');
      if (!cssName) {

        cssName = $(".schema-form-array label:contains('" + selectedSideHeading + "')").attr('class');
      }

      if (cssName.indexOf('active-acc') > 0) {
        this.onClickSideNavSub(selectedSideHeading, subHeading, index);
      }
      else {
        this.openAccordian(selectedSideHeading);
        if (subHeading) {
          this.onClickSideNavSub(selectedSideHeading, subHeading, index);
        }
      }
    }
  }

  closePopUp() {
    $('#mySystemModal').modal('hide');
  }
  deletePopUp() {
    $('#deleteEditSystem').modal('hide');

  }
  clickoptions(){
    $('#sysdropdown').toggle();
  }
}
