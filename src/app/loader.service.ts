import { Injectable } from '@angular/core';

@Injectable()
export class LoaderService {
    public showLoader: boolean = false;

    showLoading() {
        this.showLoader = true;
    }

    hideLoading() {
        this.showLoader = false;
    }
}