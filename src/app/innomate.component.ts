import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList, ChangeDetectorRef } from '@angular/core';
import { SystemService } from './system.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute, Router } from '@angular/router';
import { RoomComponent } from './room/room.component';
import { SystemComponent } from './system/system.component';
import { LoaderService } from './loader.service';
import { AuthService } from './services/auth.service';
let saveAs = require('file-saver');
import { trigger, state, style, animate, transition } from '@angular/animations';
declare var $: any;
@Component({
  selector: 'innomate',
  templateUrl: 'innomate.component.html',
  animations: [
    trigger("expandSection", [
      state("in", style({ height: "*" })),
      transition(":enter", [style({ height: 0 }), animate(100)]),
      transition(":leave", [
        style({ height: "*" }),
        animate(100, style({ height: 0 }))
      ])
    ])
  ]
})
export class InnomateComponent implements OnInit {

  constructor(private service: SystemService,
    private route: ActivatedRoute,
    private router: Router,
    public loaderService: LoaderService,
    private cdr: ChangeDetectorRef,
    private authService: AuthService) { 
      this.userName = this.authService.getUserName();
    }
  searchSystem: "";
  isRoomSelected: boolean = true;
  isStarting: boolean = false;
  isSystemSelected: boolean = false;
  checkedSystem: boolean = false;
  room_count: any;
  listRoomData: MatTableDataSource<any>;
  listSystemData: MatTableDataSource<any>;
  initialSelection = [];
  allowMultiSelect = true;
  initialSelectionR = [];
  allowMultiSelectR = true;
  selectionR = new SelectionModel<any>(this.allowMultiSelectR, this.initialSelectionR);
  selection = new SelectionModel<any>(this.allowMultiSelect, this.initialSelection);
  public userName: string;
  displayedColumns: string[] = ['select', 'room_name', 'room_status', 'room_author', 'room_version', 'system_name', 'last_deployed', 'actions'];
  displayedColumns1: string[] = ['selectSystem', 'system_name', 'rooms_count', 'system_author', 'system_version', 'last_deployed', 'actions'];

  sort: any;



  @ViewChild(MatSort, { static: false }) set content(content: ElementRef) {
    this.sort = content;
    if (this.sort) {
      if (this.listRoomData) {
        this.listRoomData.sort = this.sort;
      }
      if (this.listSystemData) {
        this.listSystemData.sort = this.sort;
      }

    }
  }
  //@ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  @ViewChild('roomPager', { static: false }) roomPager: MatPaginator;
  @ViewChild('systemPager', { static: false }) systemPager: MatPaginator;

  setDataSourceAttributes() {
    if (this.listSystemData)
      this.listSystemData.paginator = this.systemPager;

  }
  @ViewChild(RoomComponent, { static: false }) roomComponent: RoomComponent;
  @ViewChild(SystemComponent, { static: false }) systemComponent: SystemComponent;
  selectedRoom: string;
  selectedSystem: string;
  isSystemDelete: boolean = false;
  public clazz: number[] = [];
  isLoading: boolean = false;
  searchKey: string="";
  isParam: boolean = false;
  roomRepos: any[] = [];
  systemRepos: any[] = [];
  systemOtherRepos: any[] = [];
  selectedRepo; string;
  ngOnInit() {
    if (this.isMobile()) {
      this.displayedColumns = ['room_name', 'room_status', 'system_name', 'actions'];
      this.displayedColumns1 = ['system_name', 'rooms_count', 'actions'];
    }
    else {
      this.displayedColumns = ['select', 'room_name', 'room_status', 'room_author', 'room_version', 'system_name', 'last_deployed', 'actions'];
      this.displayedColumns1 = ['selectSystem', 'system_name', 'rooms_count', 'system_author', 'system_version', 'last_deployed', 'actions'];
    }
    this.getRooms();
    this.selectionR.clear();
    this.selection.clear();
  }
  reloadRooms($event) {
    console.log("event=", $event);
    if ($event == "deploy") {
      this.showMessage("Deployed Room successfully!", false, 2000);
    }
    else if ($event == "delete") {
      this.showMessage("Deleted Room successfully!", false, 2000);
    }
    else if ($event == "close") {
    }
    this.getRooms();
    this.selectionR.clear();
    this.selection.clear();
  }
  reloadSystems($event: any) {
    console.log("event=", $event);
    if ($event == "deploy") {
      this.showMessage("Deployed System successfully!", false, 2000);
    }
    else if ($event == "delete") {
      this.showMessage("Deleted System successfully!", false, 2000);
    }
    else if ($event == "close") {
    }

    this.getSystems();
    this.selectionR.clear();
    this.selection.clear();
  }
  filterRoomBySyatem(system_name:any)
  {
    this.searchKey=system_name;
    this.onRoomSelectedOnFilter();   
  }
  getRooms(withFilter = false) {

    this.roomRepos = [];
    this.isStarting = false;
    this.showLoading();
    this.service.editRoom().subscribe(schema => {

      this.hideLoading();
      let jsonData = JSON.parse(JSON.stringify(schema));
      for (let element in jsonData) {
        let key = element;
        let value = jsonData[element];
        if (value.room_status == 'Starting') {
          this.isStarting = true;
        }
        this.roomRepos.push(value);
      }
      if (this.isStarting) {
        this.onRoomSelected();
      }
      this.listRoomData = new MatTableDataSource(this.roomRepos);
      this.listRoomData.sort = this.sort;
      this.listRoomData.paginator = this.roomPager;

      this.setupFilter(withFilter);
      this.listRoomData.sortingDataAccessor = (item, property) => {     
       this.ActionShowHideJquery(); 
       return item[property];
      };
      if(withFilter){        
        this.applyRoomFilterBySystemName();
      }
      this.cdr.detectChanges();
      setTimeout(() => { this.LoadJquery(); }, 100);
    });
  }
  
  getSystems() {
    this.showLoading();
    this.service.editSystem().subscribe(schema1 => {
      this.hideLoading();
      let jsonData1 = JSON.parse(JSON.stringify(schema1));
      let systems=[];
      for (let element1 in jsonData1) {
        let value1 = jsonData1[element1];
        systems.push(value1);      
      }
      this.setSystemDataTable(systems);
    });

  }

  getSystemsList() {
    this.showLoading();
    this.systemRepos = [];
    this.systemOtherRepos = [];
    this.service.editSystem().subscribe(schema1 => {
      this.hideLoading();
      let jsonData1 = JSON.parse(JSON.stringify(schema1));      
      let systems = [];
      for (let element1 in jsonData1) {
        let value1 = jsonData1[element1];
        systems.push(value1);
      }
      systems.sort(function (a, b) {        
        if (a.rooms_count > b.rooms_count) return -1;
        if (a.rooms_count < b.rooms_count) return 1;
        return 0;
      });      
      let i = 0;
      systems.forEach((k)=>{     

        if (i > 5) {
          this.systemOtherRepos.push(k);
        }else{
        this.systemRepos.push(k);
        }
        i++;
      });
      this.cdr.detectChanges();
    });
  }

  setSystemDataTable(systems:any[]) {

    this.listSystemData = new MatTableDataSource(systems);
    this.listSystemData.sort = this.sort;
    this.listSystemData.paginator = this.systemPager;
    this.searchSystem = "";
    this.listSystemData.filterPredicate = (data, filter) => {
      return this.displayedColumns1.some(ele => {
        return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
      });
    };
    this.listSystemData.sortingDataAccessor = (item, property) => {     
      this.ActionShowHideJquery(); 
      return item[property];
     };
    setTimeout(() => { this.LoadJquery(); }, 100);
  }

  addNewRoom() {
    this.searchSystem="";
    this.getSystemsList();
  }
  onPaginateChange(event: any) {
    this.ActionShowHideJquery();
  }
  ActionShowHideJquery() {
    $("mat-row").hover(function () {
      $(this).find(".action-links").removeClass("hide-clss");
    }, function () {
      $(this).find(".action-links").addClass("hide-clss");
    });
  }
  LoadJquery() {
    this.ActionShowHideJquery();
    let searchWrapper = $('.search-wrapper');
    var searchInput = $('.search-input');
    searchWrapper.off().on("click", function (e) {

      if (~e.target.className.indexOf('search')) {
        searchWrapper.addClass('focused');
        searchInput.focus();
      }
    });
    searchInput.focusout(function () {
      searchWrapper.removeClass('focused');
    });


  }
  onSearchRoomClear() {
    this.searchKey = "";
    this.applyRoomFilter();
  }

  onSearchSystemClear() {
    this.searchKey = "";
    this.applySystemFilter();
  }

  applyRoomFilter() {   
    this.setupFilter(false);
    this.listRoomData.filter = this.searchKey.trim().toLowerCase();
    setTimeout(() => { this.ActionShowHideJquery(); }, 100);
  }
  applyRoomFilterBySystemName() {   
    this.setupFilter(true);
    this.listRoomData.filter = this.searchKey.trim().toLowerCase();
    setTimeout(() => { this.ActionShowHideJquery(); }, 100);
  }
  setupFilter(withFilter=false) {
    this.listRoomData.filterPredicate = (data, filter) => {
      if(withFilter){
        this.listRoomData.filterPredicate = function(data, filter: string): boolean {
          return data.system_name.toLowerCase()==filter;
      };
      }else{
      return this.displayedColumns.some(ele => {
        return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
      });
    }
    };
  }
  applyRoomFilter1(key: string) {
    if (key == '-----Select All Rooms-----') {
      this.ngOnInit();
    } else {
      this.listRoomData.filter = key.trim().toLowerCase();
    }
  }

  isAllRoomSelected() {
    if (this.selectionR.selected.length > 0) {
      const numSelected = this.selectionR.selected.length;
      const numRows = this.listRoomData.data.length;
      return numSelected == numRows;
    }
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterRoomToggle() {
    this.isAllRoomSelected() ?
      this.selectionR.clear() :
      this.listRoomData.data.forEach(row => this.selectionR.select(row));
  }

  applySystemFilter() {
    this.listSystemData.filter = this.searchKey.trim().toLowerCase();
    setTimeout(() => { this.ActionShowHideJquery(); }, 100);
  }

  applySystemFilter1(key: string) {
    if (key == '-----Select All Systems-----') {
      this.ngOnInit();
    } else {
      this.listSystemData.filter = key.trim().toLowerCase();
    }
  }

  isAllSystemSelected() {
    this.checkedSystem = false;
    if (this.selection.selected.length > 0) {
      for (var i = 0; i < this.selection.selected.length; i++) {
        this.room_count = this.selection.selected[i].rooms_count;
        if (this.room_count > 0) {
          this.checkedSystem = true;
        }
      }
      const numSelected = this.selection.selected.length;
      const numRows = this.listSystemData.data.length;
      return numSelected == numRows;
    }
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterSystemToggle() {
    this.isAllSystemSelected() ?
      this.selection.clear() :
      this.listSystemData.data.forEach(row => this.selection.select(row));
  }

  deleteRoom() {
    $('#deleteRoom').modal('hide');
    this.showLoading();
    this.service.deleteRoom(this.selectedRepo, this.selectedRepo).subscribe(data => {
      this.showMessage("Deleted Room successfully!", false, 3000);
      this.onRoomSelected();
      this.service.deleteRoomVC4().subscribe(info =>{
        
      })
    });

  }



  deleteRoomAll(selected1: any, selected2: any) {
    $('#deleteRoomAll').modal('hide');
    this.showLoading();
    var roomname: string[] = [];
    for (var i = 0; i < selected1.length; i++) {
      roomname.push(selected1[i].repository);
    }
    var reponame: string[] = [];
    for (var i = 0; i < selected2.length; i++) {
      reponame.push(selected2[i].repository);
    }
    let strroom: string;
    let strroom2: string;
    strroom = roomname.toString();
    strroom2 = reponame.toString();
    this.service.deleteRoom(strroom, strroom2).subscribe(data => {
      // this.ngOnInit();
      this.onRoomSelected();
      this.service.deleteRoomVC4().subscribe(info =>{
        
      })
    });

  }


  onClickEditRoom(room: string) {
    this.isLoading = true;
    console.log("room : ", room);
    this.selectedRoom = room;
    if (this.roomComponent.forTheFirstTime) {
      setTimeout(() => {
        this.isLoading = false;
      }, 3000);
    } else {
      setTimeout(() => {
        this.isLoading = false;
      }, 800);
    }
    this.roomComponent.removeActiveClasses();
    this.roomComponent.openNewSideDiv = false;
    this.roomComponent.operation = 'edit';
    this.roomComponent.loadSelectedSchema(room);
  }

  onSelectRoom(room: string, repo: string) {
    this.selectedRepo = repo;
    this.selectedRoom = room;
  }

  onClickNewRoom(room: string) {
    this.isLoading = true;
    console.log("rooooom" + room);
    if (this.roomComponent.forTheFirstTime) {
      setTimeout(() => {
        this.isLoading = false;
      }, 3000);
    } else {
      setTimeout(() => {
        this.isLoading = false;
      }, 800);
    }
    this.roomComponent.removeActiveClasses();
    this.roomComponent.forOpenDivBoolean = false;
    this.roomComponent.openNewSideDiv = false;
    this.roomComponent.operation = 'create';
    $('#selectRoom').modal('hide');
    this.roomComponent.loadSelectedSchema(room);

  }

  onClickEditSystem(system: string) {
    this.isLoading = true;
    console.log("system : ", system);
    this.selectedSystem = system;
    if (this.systemComponent.forTheFirstTime) {
      setTimeout(() => {
        this.isLoading = false;
      }, 3000);
    } else {
      setTimeout(() => {
        this.isLoading = false;
      }, 800);
    }
    this.systemComponent.removeActiveClasses();
    this.systemComponent.openNewSideDiv = false;
    this.systemComponent.operation = 'edit';
    this.systemComponent.loadSelectedSystem(system);
  }


  onClickNewSystem() {
    this.isLoading = true;

    if (this.systemComponent.forTheFirstTime) {
      setTimeout(() => {
        this.isLoading = false;
      }, 3000);
    } else {
      setTimeout(() => {
        this.isLoading = false;
      }, 800);
    }
    this.systemComponent.removeActiveClasses();
    this.systemComponent.forOpenDivBoolean = false;
    this.systemComponent.openNewSideDiv = false;
    this.systemComponent.operation = 'create';
    let system = '';
    this.systemComponent.loadSelectedSystem(system);

  }

  showLoading() {
    this.isLoading = true;
  }

  hideLoading() {
    this.isLoading = false;
  }

  onRoomSelected() {
    this.searchKey="";
    this.isRoomSelected = true;
    this.isSystemSelected = false;
    this.getRooms();
    this.selectionR.clear();
    this.selection.clear();
  }
  onRoomSelectedOnFilter() {
    this.isRoomSelected = true;
    this.isSystemSelected = false;
    this.getRooms(true);
    this.selectionR.clear();
    this.selection.clear();
  }

  onSystemSelected() {
    this.searchKey="";
    this.isRoomSelected = false;
    this.isSystemSelected = true;
    this.getSystems();
    this.selectionR.clear();
    this.selection.clear();
  }

  onSelectSystem(system: string, rooms_count: number) {
    if (rooms_count == 0) {
      this.isSystemDelete = true;
      this.selectedSystem = system;
    } else {
      this.isSystemDelete = false;
      this.selectedSystem = "";
    }


  }

  deleteSystem() {
    this.showLoading();
    $('#deleteSystem').modal('hide');
    this.service.deleteSysRooms(this.selectedSystem, this.selectedSystem).subscribe(data1 => {
    this.service.deleteSystem(this.selectedSystem, this.selectedSystem).subscribe(data => {
      this.showMessage("Deleted System successfully!", false, 2000);
      this.onSystemSelected();
      this.service.deleteSystemVC4().subscribe(info =>{
        
      })
    })
  });
  }

  deleteSystemAll(selected1: any, selected2: any) {
    this.showLoading();
    $('#deleteSystemAll').modal('hide');
    var systemname: string[] = [];
    for (var i = 0; i < selected1.length; i++) {
      systemname.push(selected1[i].repository);
    }
    var reponame: string[] = [];
    for (var i = 0; i < selected2.length; i++) {
      reponame.push(selected2[i].repository);
    }
    let strroom: string;
    let strroom2: string;
    strroom = systemname.toString();
    strroom2 = reponame.toString();
    this.service.deleteSysRooms(this.selectedSystem, this.selectedSystem).subscribe(data1 => {
    this.service.deleteSystem(strroom, strroom2).subscribe(data => {
      this.onSystemSelected();
      this.service.deleteSystemVC4().subscribe(info =>{
        
      })
    })
  });
  }

  private isMobile() {
    if (window.screen.width < 500) {
      return true;
    }
    return false;
  }

  closeAddNewRoom(){
    this.searchSystem="";
  }
  showMessage(msg: any, isError: boolean, ms: number) {
    var x = $("#show-msg");
    x.text(msg);
    x.addClass("show");
    if (isError) {
      $("#show-msg").css('background-color', "#f44259").css('color', "#fff");
    }
    else
      $("#show-msg").css('background-color', "#07ddc3").css('color', "#000");;
    setTimeout(function () { x.removeClass("show"); }, ms);
  }
  logout(): void {
    this.router.navigate(['logout']);
  }
}
