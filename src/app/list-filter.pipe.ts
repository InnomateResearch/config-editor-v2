import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'listFilter'
})
export class ListFilterPipe implements PipeTransform {

  transform(list: any[], filterText: string): any {
    if(filterText==undefined || filterText=="" || filterText==null)
    {
      return list;
    }
    return list ? list.filter(item => item.system_name.search(new RegExp(filterText, 'i')) > -1) : [];
  }

}
