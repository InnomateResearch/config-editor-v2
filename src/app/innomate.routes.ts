import { Route } from '@angular/router';

import { InnomateComponent } from './innomate.component';
import { AuthGuardService } from './services/auth-guard.service';
import {LogoutComponent} from './logout/logout.component'
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';

export const routes: Route[] = [
  { 
  path: '', 
  component: InnomateComponent,
  canActivate: [AuthGuardService]
  },
  { 
    path: 'home', 
    component: InnomateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'logout',
    component: LogoutComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'auth-callback',
    component: AuthCallbackComponent
  }
];
