import { Component, OnInit, ViewChild, Input, AfterViewInit, Output, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { MatDialog } from '@angular/material';
import { LoaderService } from '../loader.service';
let saveAs = require('file-saver');
import { AuthService } from '../services/auth.service';
import { Title } from '@angular/platform-browser';
import { SystemService } from '../system.service';
declare var $: any;


@Component({
  selector: "app-room",
  templateUrl: "./room.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("expandSection", [
      state("in", style({ height: "*" })),
      transition(":enter", [style({ height: 0 }), animate(100)]),
      transition(":leave", [
        style({ height: "*" }),
        animate(100, style({ height: 0 }))
      ])
    ])
  ],
  styleUrls: ["./room.component.scss"]
})
export class RoomComponent implements OnInit {
  @Output() reloadEvent = new EventEmitter<string>();
  @Input() selectedRoom: any;
  @Input() operation: any;
  roomSchemaList: any;
  isLoading: boolean = false;
  forOpenDivBoolean: boolean = false;
  files: File[] = [];
  forTheFirstTime: boolean = false;
  sideNavobj = new Array();
  systemValue = new Array();
  openNewSideDiv: boolean = false;
  selectedSideNavItem: any;
  forSideNav: any;
  previous: any;
  systemData: any;
  public userName: string;
  deployMsg: any;
  repoValue: string;
  repos: any[] = [];
  isSaved: boolean = true;
  showfulljson: boolean = false;
  roomRepos: any[] = [];
  deploySetName = "";
  errorDeploy: boolean = false;
  selectedIndex: any;
  isShowError: string;
  //CSS frame works
  frameworkList: any = [
    "material-design",
    "bootstrap-3",
    "bootstrap-4",
    "no-framework"
  ];
  frameworks: any = {
    "material-design": "Material Design",
    "bootstrap-3": "Bootstrap 3",
    "bootstrap-4": "Bootstrap 4",
    "no-framework": "None (plain HTML)"
  };
  selectedSetName = "";
  selectedSetSyatemName = "";
  selectedFramework = "bootstrap-4";

  selectedRepo = "";
  selectedRoomRepo = "";
  duplicateRoom = "";
  visible = {
    options: true,
    schema: true,
    form: true,
    output: true
  };

  formActive = false;
  jsonFormValid = false;
  jsonFormStatusMessage = "Loading form...";
  jsonFormSchema: any;
  schemaObject: any;
  systemName: any;

  //json schema form options
  jsonFormOptions: any = {
    addSubmit: false, // Add a submit button if layout does not have one
    //addTitle: 'Save' ,
    debug: false, // Don't show inline debugging information
    loadExternalAssets: true, // Load external css and JavaScript for frameworks
    returnEmptyFields: false, // Don't return values for empty input fields
    fieldsRequired: true,
    setSchemaDefaults: true, // Always use schema defaults for empty fields
    defautWidgetOptions: { feedback: true } // Show inline feedback icons
  };

  errorMsg: any[] = [];
  showErrors: boolean = false;
  formIsValid = null;
  keysLength = 0;
  schemaKeysList: any;
  disableProperties: string[] = ['System Name', 'System Repository Name', 'System Description', 'System Version', 'System Author'];
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private service: SystemService,
    private dialog: MatDialog,
    public loaderService: LoaderService,
    private authService: AuthService,
    private titleService: Title,
    private systemService: SystemService,
    private cdr: ChangeDetectorRef
  ) {
    this.userName = this.authService.getUserName();
    this.titleService.setTitle("Innomate Room Configuration");
    console.log("construtror called");
  }

  logout(): void {
    this.router.navigate(["logout"]);
  }
  reloadRooms(callBackEvent) {
    this.showErrors = false;
    this.reloadEvent.next(callBackEvent);
  }
  ngOnInit() {
    this.forTheFirstTime = true;
    console.log("onintit called");
    // Subscribe to query string to detect schema to load
    this.route.queryParams.subscribe(params => {
      this.selectedRepo = params.page;
      this.selectedRoomRepo = params.roomName;
      // this.loadnewSchema(this.selectedRepo);
    });
  }
  //On load



  // openDialog(): void {
  //   this.dialog.open(this.dialogRef);
  // }
  duplicateConfig() {
    this.duplicateRoom = this.selectedRoomRepo;
    this.selectedRoomRepo = "";
    this.operation = 'create';
    this.jsonFormSchema.properties.Properties.properties[
      "Room Name"
    ].readonly = false;
    this.jsonFormSchema.properties.Properties.properties[
      "Room Repository Name"
    ].readonly = false;
    this.isSaved = false;
    let schema = JSON.stringify(this.jsonFormSchema);
    let system = JSON.stringify(this.systemData);
    this.generateFormFull(schema, system);
    this.showMessage(
      'The configuration for [' + this.duplicateRoom + '] has been successfully duplicated., Please "save" it.',
      false,
      3000
    );
  }
  checkSpecialChar(str) {
    var format = /[!@#$%^&*()_+\=\[\]{};':"\\|,.<>\/?]+/;

    if (format.test(str)) {
      return true;
    } else {
      return false;
    }
  }
  hasLowerCase(str) {
    return /[A-Z]/.test(str);
  }

  //save the System
  onSave(type: any) {
    if(type=="deploy"){
      $('#myModal').modal('hide');
    }
    this.isLoading = true;
    let repoName = this.systemData.Properties["Room Repository Name"];
    let rmName = this.systemData.Properties["Room Name"];
    this.deploySetName = rmName;
    if (this.errorMsg.length > 0) {
      this.showErrors = true;
      this.isLoading = false;
      return;
    } else {

      this.showErrors = false;
    }
    if (
      repoName.includes(" ") ||
      this.hasLowerCase(repoName) ||
      this.checkSpecialChar(repoName)
    ) {
      this.errorMsg.push({ at: "Properties", msg: "Entered Room Repository Name should not contain spaces/uppercase/special characters" });
      $('.error-css').show();
      this.showErrors = true;
      this.isLoading = false;
      return;
    }

    if (this.selectedRoomRepo == "" || this.selectedRoomRepo == undefined) {
      this.systemService.roomRepositoryList().subscribe(
        repolist => {
          let repositoriesName = JSON.parse(JSON.stringify(repolist));
          for (let element in repositoriesName) {
            let key = repositoriesName[element].repository;
            let value = repositoriesName[element].room_name;
            if (repoName == key) {
              this.isLoading = false;
              this.errorMsg.push({ at: "Properties", msg: "Entered Room Repository Name Already Exists" });
              $('.error-css').show();
              this.showMessage('Entered Room Repository Name Already Exists', true, 3000);
              this.showErrors = true;
              this.isLoading = false;
              this.hideLoading();
              return;
            }
            if (rmName == value) {
              this.isLoading = false;
              this.errorMsg.push({ at: "Properties", msg: "Entered Room Name Already Exists" });
              $('.error-css').show();
              this.showMessage('Entered Room Name Already Exists', true, 3000);
              this.showErrors = true;
              this.isLoading = false;
              this.hideLoading();
              return;
            }
          }
          let repo_details = {
            "room_name": this.systemData.Properties["Room Name"],
            "system_name": this.systemData.Properties["System Name"],
            "system_repo_name": this.systemData.Properties["System Repository Name"],
            "room_author": this.systemData.Properties["Room Author"],
            "room_version": this.systemData.Properties["Room Version"]
          }
          this.systemService
            .createRoom(repoName, JSON.stringify(repo_details))
            .subscribe(
              schema => {
                this.systemService
                  .commitfile(repoName, "room.json", JSON.stringify(this.systemData))
                  .subscribe(
                    data => {
                      this.isSaved = false;


                      this.showMessage(
                        '"' + rmName + '" created successfully!',
                        false,
                        3000
                      );
                      if (type == "deploy") {
                        this.deploy();
                      }
                      else {
                        this.operation = 'edit';
                        this.loadSelectedSchema(repoName);
                      }

                    },
                    error => {
                      this.isLoading = false;
                      this.deployMsg = error.error;
                      $(".deploy-css").show();
                      this.showMessage(error.error, true, 3000);
                    }
                  );
              },
              error => {
                this.isLoading = false;
                this.deployMsg = error.error;
                $(".deploy-css").show();
                this.showMessage(error.error, true, 3000);
              }
            );
        },
        error => {
          this.isLoading = false;
          this.deployMsg = error.error;
          $(".deploy-css").show();
          this.showMessage(error.error, true, 3000);
        }
      );
    } else {
      this.systemService
        .commitfile(repoName, "room.json", JSON.stringify(this.systemData))
        .subscribe(
          data => {
            this.isSaved = false;
            this.showMessage(
              '"' + rmName + '" saved successfully!',
              false,
              3000
            );
            if (type == "deploy") {
              this.deploy();
            }
            else {
              this.operation = 'edit';
              this.loadSelectedSchema(repoName);
            }
          },
          error => {
            this.isLoading = false;
            this.deployMsg = error.error;
            $(".deploy-css").show();
            this.showMessage(error.error, true, 3000);
          }
        );
    }
  }
  onDeploy(){
    this.deploySetName = this.systemData.Properties["Room Name"];
    $('#myModal').modal('show');
  }
  deploy() {
    let repoName = this.systemData.Properties["Room Repository Name"];
    let rmName = this.systemData.Properties["Room Name"];
    let systemName = this.systemData.Properties["System Name"];
    this.isLoading = true;
    let iserror = false;
    this.errorDeploy = false;
    
    this.systemService.roomDeploy(rmName, repoName, systemName).subscribe(
      data => {

        //this.isLoading = false;
        for (let i = 0; i < data.length; i++) {
          if (data[i].includes("not available on VC4")) {
            console.log(data[i]);
            iserror = true;
          }
        }
        this.isLoading = false;
        if (iserror) {
          this.systemName = systemName;
          this.deploySetName = rmName;
          this.errorDeploy = true;
          this.isLoading = false;
          this.cdr.detectChanges();
          $('#myModal').modal('show');
          //this.showMessage("System: "+systemName + " Not Available on VC4", true, 3000);
        } else {
          $('#editRoom').modal('hide');
          this.reloadRooms("deploy");
        }
      },
      error => {
        this.isLoading = false;
        console.log(error.status);
        if (error.status == '400') {
          this.deployMsg = "The system [" + systemName + "] is not deployed to the Virtual Control servers. Please deploy the system first, before deploying room [" + rmName + "] that reference this system.";
        } else if (error.status == '500') {
          this.deployMsg = " VC4 was not responding";
        } else if (error.status == '204') {
          this.deployMsg = "room.json file missing";
        } else {
          this.deployMsg = error.error;
        }
        $(".deploy-css").show();
        this.showMessage(this.deployMsg, true, 5000);
        this.operation = 'edit';
        this.loadSelectedSchema(repoName);
      }
    );

  }

  //revert changes

  revertChanges() {

    this.isSaved = false;
    this.loadSelectedSchema(this.selectedRoom);
    this.showMessage("Reverted changes successfully!", false, 3000);
  }

  //discard changes
  discardChanges() {
    this.router.navigateByUrl("");
  }
  //download file
  onClickDownloadFile(event: any, data: any) {
    let file = new Blob([JSON.stringify(data)], {
      type: "application/json;charset=utf-8"
    });
    saveAs(file, `room.json`);
  }

  //form submit data
  onSubmit(data: any) {
    let file = new Blob([JSON.stringify(data)], {
      type: "application/json;charset=utf-8"
    });
    saveAs(file, `room.json`);
  }

  isValid(isValid: boolean): void {
    this.formIsValid = isValid;
  }

  validationErrors(data: any): void {
    this.errorMsg = [];

    if (data) {
      data.forEach(element => {
        let msg = "";
        let propertyName = "";
        let errAt = element["dataPath"]
          .toString()
          .slice(1)
          .replace(/\//g, ">>");
        if (element["keyword"] == "required") {
          propertyName = element.params.missingProperty;
          msg = "<b>" + propertyName + "</b> is required"
        }
        else {
          if (element["message"] != undefined) {
            msg = element["message"];
            msg = msg.replace("'", "<b>");
            msg = msg.replace("'", "</b>");
          }
        }

        this.errorMsg.push({ at: errAt, msg: msg });
      });
    } else {
      this.showErrors = false;
      $(".deploy-css").hide();
    }
  }

  //On load
  loadSelectedSchema(selectedRepo) {
    this.openNewSideDiv = false;
    this.keysLength = 0;
    this.jsonFormStatusMessage = "Loading form...";
    this.formActive = false;
    this.isShowError = "";
    this.jsonFormSchema = {};
    this.sideNavobj = [];
    this.selectedRoom = selectedRepo;
    this.schemaObject = [];
    this.isLoading = true;
    this.errorDeploy = false;
    $('#dropdown').hide();
    if (this.operation == "create") {
      console.log("in if create");
      this.isLoading = true;

      this.selectedRoomRepo = "";
      this.systemService.getRoomSchema(selectedRepo).subscribe(
        schema => {
          this.schemaObject = JSON.parse(JSON.stringify(schema));
          this.systemService.filterSystem(selectedRepo).subscribe(
            data => {
              let jsonData = JSON.parse(JSON.stringify(data));
              let tempJson = {};

              Object.keys(this.schemaObject.properties).forEach((k: any, i: any) => {
                tempJson[k] = jsonData[k];
              });
              this.selectedSetSyatemName = jsonData.Properties["System Name"];

              let stringify = JSON.stringify(tempJson);
              this.generateFormFull(JSON.stringify(schema), stringify);
            },
            error => {
              this.isLoading = false;
              this.deployMsg = error.error;
              $(".deploy-css").show();
              this.showMessage(error.error, true, 3000);
            }
          );
        },
        error => {
          this.isLoading = false;
          this.deployMsg = error.error;
          $(".deploy-css").show();
          this.showMessage(error.error, true, 3000);
        }
      );
      // this.isLoading = false;
    } else {
      // console.log("in else",this.selectedRoom);

      // this.selectedRepo=this.selectedRoom?this.selectedRoom:'pod1';
      console.log("------->", selectedRepo);
      this.isLoading = true;
      this.systemService.filterRoom(selectedRepo).subscribe(
        data => {
          //console.log("data :", data);
          let jsonData = JSON.parse(JSON.stringify(data));
          //  console.log("jsonData : ", jsonData);

          this.selectedSetSyatemName = jsonData.Properties["System Name"];
          this.selectedRepo = jsonData.Properties["System Repository Name"];
          this.showfulljson =
            jsonData.Properties["Is Full Configuration"] == true;

          this.selectedRoomRepo = jsonData.Properties["Room Repository Name"];
          this.selectedSetName = jsonData.Properties["Room Name"];

          //const schemaURL = environment.API+`/fetchfile/?repository=` + this.selectedRepo + `&file_name=`+environment.MAIN_SCHEMA;
          this.systemService.getRoomSchema(this.selectedRepo).subscribe(
            schema => {
              this.generateFormFull(
                JSON.stringify(schema),
                JSON.stringify(data)
              );

              // this.isLoading = false;
            },
            error => {
              this.isLoading = false;
              this.deployMsg = error.error;
              $(".deploy-css").show();
              this.showMessage(error.error, true, 3000);
            }
          );
          // this.isLoading = false;
        },
        error => {
          this.isLoading = false;
          this.isLoading = false;
          this.deployMsg = error.error;
          $(".deploy-css").show();
          this.showMessage(error.error, true, 3000);
        }
      );
    }
    //this.isLoading = false;
    this.forTheFirstTime = false;
    //setTimeout(() => {
    // this.onClickKey('Properties','');
    // }, 3000);
  }

  //end
  toggleSchemaItem(event) {

  }
  generateFormFull(schema: string, system: string) {

    this.jsonFormSchema = {};
    this.systemData = {};
    this.formActive = false;
    if (!schema) {
      console.log("schema is empty");
      return;
    }
    console.log("generate formfull")
    this.jsonFormStatusMessage = "Loading form...";

    try {

      let schemaObj = JSON.parse(schema);

      if (Object.keys(schemaObj.properties).length === 0) {
        this.jsonFormStatusMessage = "System properties are undefined.";
      }
      else {
        this.disableProperties.forEach(k => {
          try {
            if (schemaObj.properties.Properties != null) {
              schemaObj.properties.Properties.properties[k].readonly = true;
            }
          } catch (e) {
            console.log(e);
          }
        });
        this.schemaKeysList = Object.keys(schemaObj.properties);
        this.keysLength = this.schemaKeysList.length;
        this.schemaKeysList.forEach((k, i) => {
          if (i == 0) {
            schemaObj.properties[k]["htmlClass"] = "cust-label-class" + i;
          }
          else {
            schemaObj.properties[k]["htmlClass"] = "cust-label-class" + i + " prop-hide";
          }

        });


        if (this.selectedRoomRepo != "") {
          schemaObj.properties.Properties.properties[
            "Room Name"
          ].readonly = true;
          schemaObj.properties.Properties.properties[
            "Room Repository Name"
          ].readonly = true;
        }

        this.jsonFormSchema = schemaObj;
        this.systemData = JSON.parse(system);
        this.generateSideNav();
        this.loadFileTree();
        this.jsonFormValid = true;
        this.formActive = true;
      }

      this.isLoading = false;
      this.cdr.detectChanges();
    } catch (jsonError) {
      this.isLoading = false;
      this.formActive = false;
      this.jsonFormValid = false;
      this.jsonFormStatusMessage =
        "Entered content is not currently a valid JSON Form object.\n" +
        "As soon as it is, you will see your form here. So keep typing. :-)\n\n" +
        "JavaScript parser returned:\n\n" +
        jsonError;
      this.showMessage(jsonError, true, 3000);
      return;
    }

    // setTimeout(() => {
    //   this.Jqhide();
    // }, 100);
  }

  uploadFile(event) {
    if (this.files.length !== 1) {
      console.error("No file selected");
    } else {

      const reader = new FileReader();
      reader.onloadend = e => {
        this.isLoading = true;
        this.isSaved = false;
        let jsonData;
        try {
          jsonData = JSON.parse(reader.result.toString());
        } catch (e) {
          console.log(e);
          this.isLoading = false;
          this.deployMsg = "Exception while loading file";
          this.showMessage(this.deployMsg, true, 3000);
          return;
        }

        jsonData.Properties["System Name"] = this.systemData.Properties[
          "System Name"
        ];
        jsonData.Properties[
          "System Repository Name"
        ] = this.systemData.Properties["System Repository Name"];

        this.generateFormFull(
          JSON.stringify(this.schemaObject),
          JSON.stringify(jsonData)
        );


        this.showMessage(
          "New configuration has loaded. Please 'Save' it, to persist.",
          false,
          3000
        );
      };
      reader.readAsText(this.files[0]);
    }
    $('#uploadroomFile').modal('hide');
  }
  process(key: any, value: any) {
    if (typeof value == "object") {
      let requiredArr = [];
      if (value.required) {
        requiredArr = value.required;
      }
      if (value.properties) {
        let schemaKeys = Object.keys(value.properties);
        schemaKeys.forEach(element => {
          let ct = 0;

          requiredArr.forEach(el => {
            if (el == element) {
              ct++;
            }
          });

          if (
            value.properties[element].type != "array" &&
            value.properties[element].type != "object"
          ) {
            if (ct == 0) {
              delete value.properties[element];
            }
          }
        });
      }
    }
  }

  traverse(o: any, func: any) {
    for (var i in o) {
      func.apply(this, [i, o[i]]);
      if (o[i] !== null && typeof o[i] == "object") {
        //going one step down in the object tree!!
        let jsonStr = JSON.stringify(o[i]);
        if (
          i !== "required" &&
          !jsonStr.includes('"required"') &&
          (o[i].type == "object" || o[i].type == "array")
        ) {
          delete o[i];
        } else {
          this.traverse(o[i], func);
        }
      }
    }
  }

  RemoveStringType() {
    let schemaKeys = Object.keys(this.schemaObject.properties);
    schemaKeys.forEach(element => {
      if (this.schemaObject.properties[element].type == "string") {
        delete this.schemaObject.properties[element];
      }
    });
  }

  showLoading() {
    this.isLoading = true;
  }

  hideLoading() {
    this.isLoading = false;
  }

  onLoadRoomFile() {
    this.files = [];
  }
  showMessage(msg, isError, ms) {
    console.log("error=", msg)
    var x = document.getElementById("snackbar");
    this.isShowError = msg;
    x.className = "show";
    if (isError) {
      $("#snackbar")
        .css("background-color", "#f44259")
        .css("color", "#fff");
    } else
      $("#snackbar")
        .css("background-color", "#07ddc3")
        .css("color", "#000");
    this.cdr.detectChanges();
    setTimeout(function () {
      this.isShowError = "";
    }, ms);
  }
  toggleVisible(item: string) {
    this.visible[item] = !this.visible[item];
  }

  toggleFormOption(option: string) {
    if (option === "feedback") {
      this.jsonFormOptions.defautWidgetOptions.feedback = !this.jsonFormOptions
        .defautWidgetOptions.feedback;
    } else {
      this.jsonFormOptions[option] = !this.jsonFormOptions[option];
    }

    this.generateFormFull(
      JSON.stringify(this.jsonFormSchema),
      JSON.stringify(this.systemData)
    );
  }
  onCreateSideNav() {
    let sideNav = [];
    let side = "n";
    let bottom = "n";
    let pro = this.jsonFormSchema.properties;
    Object.keys(pro).forEach(function (k) {
      if (pro[k].type == "object") {
        let finalItem = [];
        let item = Object.keys(pro[k].properties);

        for (let i = 0; i < item.length; i++) {
          if (pro[k].properties[item[i]].type == "array" || pro[k].properties[item[i]].type == "object") {
            bottom = "y";
            finalItem.push({ key: item[i], value: "" });
          }
        }
        sideNav.push({ key: k, items: finalItem, side: side, bottom: bottom });

      }
      if (pro[k].type == "array") {
        sideNav.push({ key: k, items: [], side: side, bottom: bottom });
      }

    });
    return sideNav;
  }

  loadFileTree() {
    $(document).ready(function () {
      $(".pull-right").each(function () {
        if ($(this).find("span").html()) {
          if ($(this).find("span").html().includes("Add to ")) {
            $(this).find("span").text(function (index, text) {
              return text.replace('Add to', 'Add');
            });
          }
        }
      });

      $('.submenu').hide();
      $('.modal-menu ul li a.m-head').click(function () {
        $('.submenu').slideUp(200);
        $(this).next().slideToggle(200);
      });
      $('.submenu').slideUp(200);
    });
  }

  generateSideNav() {

    let systemSideNavObj = this.systemData;
    let schemaSideNavObj = this.jsonFormSchema.properties;
    let scmSidenav = this.onCreateSideNav();
    let arr = new Array();
    Object.keys(systemSideNavObj).forEach(function (k) {
      let side = "n";
      let bottom = "n";
      let item = [];
      let finalItem = [];
      let keysArr = [];
      if (Array.isArray(systemSideNavObj[k])) {
        side = "y";
        bottom = "n";
        let data = [];
        data = Object["values"](systemSideNavObj[k]);
        for (let j = 0; j < data.length; j++) {
          if (data[j]["ID"])
            keysArr.push(data[j]["ID"]);
          else if (data[j]["Name"])
            keysArr.push(data[j]["Name"]);
        }
        item = keysArr;
        if (item.length == 0) {
          side = "n";
        }
      } else {
        side = "n";
        bottom = "n";
        if (systemSideNavObj[k]) {
          {
            let scmItems = [];
            finalItem = [];
            item = Object.keys(systemSideNavObj[k]);
            if (Array.isArray(schemaSideNavObj[k].properties))
              scmItems = schemaSideNavObj[k].properties;
            if (typeof schemaSideNavObj[k].properties === 'object')
              scmItems = Object.keys(schemaSideNavObj[k].properties);

            for (let i = 0; i < item.length; i++) {
              if (Array.isArray(systemSideNavObj[k][item[i]]) || typeof systemSideNavObj[k][item[i]] === 'object') {
                bottom = "y";
              }
            }

            if (bottom == "y") {
              for (let i = 0; i < scmItems.length; i++) {
                finalItem.push({ key: scmItems[i] });
              }
              item = finalItem;
            }
          }
        }
      }
      arr.push({ key: k, items: item, side: side, bottom: bottom });
    });
    scmSidenav.forEach((k1: any, i1: any) => {
      arr.forEach((k2: any, i2: any) => {
        if (k1.key == k2.key) {
          scmSidenav[i1] = k2;
        }
      })
    });
    this.sideNavobj = scmSidenav;

    setTimeout(() => {
      $("a.active").removeClass("active");
      $(".heading-0").addClass("active");
    }, 0);
  }


  onMenuItemClicked(heading, index) {

    //console.log("onMenuItemClicked=", heading);
    this.selectedSideNavItem = heading.key;
    let systemSideNavObj = this.systemData;
    let data = [];
    let keysArr = [];

    Object.keys(systemSideNavObj).forEach(function (k) {
      if (k === heading.key) {
        data = Object["values"](systemSideNavObj[k]);
        for (let j = 0; j < data.length; j++) {
          if (data[j]["ID"])
            keysArr.push(data[j]["ID"]);
          else if (data[j]["Name"])
            keysArr.push(data[j]["Name"]);
        }
      }
    });

    this.forSideNav = { key: heading.key, items: keysArr, side: heading.side, bottom: heading.bottom };

    $("a.active").removeClass("active");
    $(".heading-" + index).addClass("active");

    if (heading.side == "y") {
      this.openNewSideDiv = true;
    }
    else {
      this.openNewSideDiv = false;
    }

    for (let i = 0; i < this.keysLength; i++) {
      $(".cust-label-class" + i).addClass("prop-hide");
    }
    for (let i = 0; i < this.keysLength; i++) {
      if (index == i) {
        $(".cust-label-class" + i).removeClass("prop-hide");
      }
    }

    if (heading.side == "n" && heading.bottom == "n") {
      let legendFlag = true;
      $(".cust-label-class" + index).find("legend").each(function () {
        if (heading.key == $(this).text()) {
          legendFlag = false;
          $(this).parent().find('input:first').focus();
        }
      });
      if (legendFlag) {
        $(".cust-label-class" + index).find("label").each(function () {
          if (heading.key == $(this).text()) {
            $(this).parent().find('input:first').focus();
          }
        });
      }
    }

  }

  onBottomMenuItemClicked(heading, index) {
    //console.log("bottom itemheading=", heading, " index=", index);
    let systemSideNavObj = this.systemData;
    let side = "n";
    let bottom = "n";

    let data = [];
    let keysArr = [];


    this.openNewSideDiv = true;
    Object.keys(systemSideNavObj).forEach(function (k) {
      if (k === heading.key) {
        data = Object["values"](systemSideNavObj[k]);
        if (data[index]) {
          for (let j = 0; j < data[index].length; j++) {
            if (data[index][j]["ID"])
              keysArr.push(data[index][j]["ID"]);
            else if (data[index][j]["Name"])
              keysArr.push(data[index][j]["Name"]);
          }
        }
      }
    });


    let headingIndex = 0;
    this.selectedSideNavItem = heading.key;
    if (keysArr.length != 0) {
      this.openNewSideDiv = true;
    }
    else {
      this.openNewSideDiv = false;
    }
    this.forSideNav = { key: heading.key, items: keysArr, side: side, bottom: bottom, selected: heading.items[index].key };

    $("a.active").removeClass("active");
    for (let i = 0; i < this.keysLength; i++) {
      $(".cust-label-class" + i).addClass("prop-hide");
    }
    this.schemaKeysList.forEach((k: any, i: any) => {
      if (k === heading.key) {
        headingIndex = i;
        $(".cust-label-class" + i).removeClass("prop-hide");
        $(".heading-" + i).addClass("active");
      }
    });

    $(".sub-heading-" + index).addClass("active");
    if (keysArr.length == 0) {
      let legendFlag = true;
      $(".cust-label-class" + headingIndex).find("legend").each(function () {
        if (heading.items[index].key == $(this).text()) {
          legendFlag = false;
          $(this).parent().find('input[type=text]:first').focus();

        }
      });
      if (legendFlag) {
        $(".cust-label-class" + headingIndex).find("label").each(function () {
          if (heading.items[index].key == $(this).text()) {
            $(this).parent().find('input[type=text]:first').focus();

          }
        });
      }


    }
    else {

    }


  }

  onSideMenuItemClicked(heading, subHeading, index) {
    //  console.log("heading=", heading, index);
    let headingIndex = 0;
    this.schemaKeysList.forEach((k: any, i: any) => {
      if (k === heading.key) {
        headingIndex = i;
      }
    });
    $(".cust-label-class" + headingIndex).find("legend").each(function (i) {
      if (!heading.selected) {
        $(this).parent().find('input').each(function () {
          if (subHeading == $(this).val()) {
            $(this).focus();
            return false;
          }
        });;
      }
      else if (heading.selected == $(this).text()) {
        $(this).parent().find('input[type=text]').each(function () {
          if (subHeading == $(this).val()) {
            $(this).focus();
            return false;
          }
        });;
      }
    });
    heading.items.forEach((k: any, i: any) => {
      $(".sidenav-heading-" + i).removeClass("active");
    });
    $(".sidenav-heading-" + index).addClass("active");
  }
  hideAll() {
    //paragraph
    $("fieldset legend:first").hide();
    $("fieldset legend:first")
      .next("div")
      .hide();
    //$("bootstrap-3-framework div p").hide();

    $(".schema-form-section div p:first").hide();

    $(".schema-form-array div p:first").hide();
    $("bootstrap-3-framework")
      .find(".schema-form-section div:first p:first")
      .hide();
    $("bootstrap-3-framework")
      .find(".schema-form-array div:first p:first")
      .hide();

    //main heading
    $("fieldset")
      .find("legend:first")
      .hide();
    $("fieldset")
      .find("legend:first")
      .next("div")
      .hide();

    //Actual panel
    $("section-widget root-widget").hide();
  }
  addCss() {
    //$("section-widget label[class^='ng-star-inserted']:first").addClass("lbl-header");
    $("section-widget label[class^='ng-star-inserted']:first-child").addClass(
      "lbl-header"
    );
    //$(".schema-form-array label:first").addClass("lbl-header");
    $(".schema-form-array")
      .find("label:first")
      .addClass("lbl-header");

    $("bootstrap-3-framework")
      .find(".schema-form-section")
      .addClass("form-group-header");
    $("bootstrap-3-framework")
      .find(".schema-form-array")
      .addClass("form-group-header");
    $("bootstrap-3-framework")
      .find("button")
      .parent()
      .addClass("form-group-header-box");
    // console.log($('bootstrap-3-framework').find('button').parent().closest("add-reference-widget").parent().closest("bootstrap-3-framework").html());
    $("bootstrap-3-framework")
      .find("button")
      .parent()
      .closest("add-reference-widget")
      .parent()
      .closest("bootstrap-3-framework")
      .find("div:first")
      .addClass("schema-form-ref");
  }
  Jqhide() {
    $(".deploy-css").hide();

    this.hideAll();
    this.addCss();

    $("section-widget label[class^='ng-star-inserted']:first-child").bind(
      "click",
      function (e) {
        $(this).toggleClass("active-acc");
        $(this)
          .parent()
          .find("root-widget")
          .fadeToggle();
        $(this)
          .parent()
          .find("root-widget input:first")
          .focus();

        $(this)
          .parent()
          .find("root-widget label.ng-star-inserted")
          .removeClass("lbl-header")
          .unbind();
        $(this)
          .parent()
          .find("section-widget label.ng-star-inserted")
          .removeClass("lbl-header")
          .unbind();
      }
    );

    $(".schema-form-array")
      .find("label:first")
      .bind("click", function (e) {
        $(this).toggleClass("active-acc");
        $(this)
          .parent()
          .find("root-widget")
          .fadeToggle();
        $(this)
          .parent()
          .find("root-widget input:first")
          .focus();
        $(this)
          .parent()
          .find("section-widget label.ng-star-inserted")
          .removeClass("lbl-header")
          .unbind();
      });

    $(".file-tree").filetree();
    $(".file-list .folder-root").on("click", function () {
      if ($(this).hasClass("closed")) {
        $(this).removeClass("closed");
        $(this).addClass("open");
      } else {
        $(this).removeClass("open");
        $(this).addClass("closed");
      }
      return false;
    });

    if (this.isSaved) {
      this.handbeger();
    }
  }
  handbeger() {
    $(".hamburger").click(function () {
      $(this)
        .find(".dropdown-menu")
        .slideToggle("fast");
    });
    $(document).on("click", function (event) {
      var $trigger = $(".hamburger");
      if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".dropdown-menu").slideUp("fast");
      }
    });

    $(".header__menu").click(function (e) {
      e.stopPropagation(); // This is the preferred method.
      return false; // This should not be used unless you do not want
      // any click events registering inside the div
    });
  }

  openAccordian(selectedSideHeading) {
    if (selectedSideHeading) {
      $("section-widget label:contains('" + selectedSideHeading + "')").click();
      $(
        ".schema-form-array label:contains('" + selectedSideHeading + "')"
      ).click();
      // $(".schema-form-array label:contains('"+selectedSideHeading+"')").css('background-color', 'red');
    }
  }
  onClickSideNavSub(subHeading, index) {
    let isFormArray = true;

    if (subHeading) {
      if (isFormArray) {
        var dv = $(".schema-form-array label").filter(function () {
          return $(this).text() == this.selectedSideNavItem;
        }).siblings().find("label").filter(function () {
          return $(this).text().toLowerCase() == this.selectedSideNavItem.toLowerCase();
        });
        dv.each(function (el, el2) {
          if (index == el) {
            this.click();
          }
        });
      }
      else {
        $("section-widget label:contains('" + subHeading + "')").click();
      }
    }

  }
  openAccordianSub(subHeading, index) {
    //console.log(this.selectedSideNavItem, " subHeading=", subHeading.key, " index=" + index);

    this.onClickSideNavSub(subHeading.key, index);
  }
  onSelect(event) {
    //console.log(event);
    this.files.push(...event.addedFiles);
  }

  onRemove(event) {
    // console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  removeActiveClasses() {
    $("a.active").removeClass("active");
    setTimeout(() => {
      $(".heading-0").addClass("active");
      $('#inner-' + 0).css('display', 'block');
    }, 0);
  }

  deleteRoom(room_name: any, repository: any) {
    this.service.deleteRoom(repository, repository).subscribe(data => {
      $('#deleteEditRoom').modal('hide');
      this.reloadRooms("delete");
      this.service.deleteRoomVC4().subscribe(info => {

      })
    });
  }


  close() {
    this.formActive = false;
    this.isShowError = "";
    this.jsonFormSchema = {};
    this.sideNavobj = [];
    this.reloadRooms("close");
  }

  closePopUp() {
    $('#myModal').modal('hide');
  }
  deletePopUp() {
    $('#deleteEditRoom').modal('hide');

  }
  clickoptions(){
    $('#dropdown').toggle();
  }

}
