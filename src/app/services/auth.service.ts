import { Injectable, EventEmitter, Output  } from '@angular/core';

import { UserManager, UserManagerSettings, User } from 'oidc-client';
//import { AngularFireAuth } from '@angular/fire/auth';

@Injectable()
export class AuthService {
  manager: UserManager = new UserManager(getClientSettings());
  private user: User = null;
  public userName : string;
	@Output() getLoggedInName: EventEmitter<string> = new EventEmitter();
  constructor( /* public afAuth: AngularFireAuth */ ) {
    this.manager.getUser().then(user => {
      this.user = user;
	   
	    //console.log(this.user.profile.name);
    });
  }

  isLoggedIn(): boolean {
	 
    return this.user != null && !this.user.expired;
  }
  getUserName() : string {
    return this.userName;
  }
  getClaims(): any {
	this.getLoggedInName.emit(this.user.profile.name);
    return this.user.profile.name;
  }

  getAuthorizationHeaderValue(): string {
    return `${this.user.token_type} ${this.user.access_token}`;
  }
  
  startAuthentication(): Promise<void> {
	  // console.log("start authenctication");
    return this.manager.signinRedirect();
  }

  async logout(){
	  //console.log("log out auth service called");
	  this.user = null;
  }
  
  stopAuthentication(): Promise<void> {
	   console.log("stop authentication service called");
	   return this.manager.signoutRedirect();
  }
  
  completeAuthentication(): Promise<void> {
    return this.manager.signinRedirectCallback().then(user => {
      this.user = user;
      this.userName = this.user.profile.name;
    //  console.log("on successfull login"+this.user.profile.name);
	  this.getLoggedInName.emit(this.user.profile.name);
    });
  }
}

export function getClientSettings(): UserManagerSettings {
  return {
    authority: 'https://innomate-dev.onelogin.com/oidc',
    client_id: '429a2080-385f-0137-dab5-0627f60f60a0142786',
    redirect_uri: 'https://uq.innomesh.com.au/auth-callback',
    post_logout_redirect_uri: 'https://uq.innomesh.com.au/',
  //  redirect_uri: 'http://localhost:4200/auth-callback',
   // post_logout_redirect_uri: 'http://localhost:4200/login',
	//end_session_endpoint: 'http://localhost:4200/',
    response_type: 'id_token token',
    scope: 'openid profile',
    filterProtocolClaims: true,
    loadUserInfo: true,
    automaticSilentRenew: true,
    silent_redirect_uri: 'https://uq.innomesh.com.au/silent-refresh.html',
 };
}
