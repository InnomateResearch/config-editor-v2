import { Injectable } from '@angular/core';
import { Observable, Subject, pipe } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { HttpParams } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'text/plain'
  })
};
const httpJSONOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
const httpOptions1 = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded'
  })
};

@Injectable()
export class SystemService {

  public fetchAPI = environment.API + `/fetchfile/?repository=`;
  public fetchSystemAPI = `&file_name=` + environment.SYSTEM_DATA;
  public fetchRoomSchemaAPI = `&file_name=` + environment.ROOM_SCHEMA;
  public fetchRoomAPI = `&file_name=` + environment.ROOM_DATA;
  public editsystem = environment.API + `/systems/`;
  public editroom = environment.API + `/rooms/`;
  public schemaURL = environment.API + `/fetchfile/?repository=` + environment.SOFTWARE_PRJ + `&file_name=` + environment.SYSTEM_SCHEMA;
  public roomschemaURL = environment.API + `/fetchfile/?repository=` + environment.SOFTWARE_PRJ + `&file_name=` + environment.ROOM_SCHEMA;
  public schemaRepoURL = `&file_name=` + environment.SYSTEM_SCHEMA;
  public repoCreateURL = environment.API + `/repository/`;
  public schema_systemURL = environment.API + `/commitfile/`;
  public deployURL = environment.API + `/ProgramInstance/`;
  public triggerPipeline = environment.API + `/triggerPipeline/`;
  public updateRepoVariable = environment.API + `/updateRepoVariable/`;
  public edittriggerPipeline = environment.API + `/editsystemarchive/`;
  public programLib = environment.API + `/ProgramLibrary/`;



  constructor(private http: HttpClient) { }

  filterSystem(id: string): Observable<any> {
    return this.http.get(`${this.fetchAPI}${id}${this.fetchSystemAPI}`, httpOptions);
  }

  filterRoom(id: string): Observable<any> {
    return this.http.get(`${this.fetchAPI}${id}${this.fetchRoomAPI}`, httpOptions);
  }

  filterRoomSchema(id: string): Observable<any> {
    return this.http.get(`${this.fetchAPI}${id}${this.fetchRoomSchemaAPI}`, httpOptions);
  }

  editSystem(): Observable<any> {
    return this.http.get(`${this.editsystem}`, httpJSONOptions);
  }

  editRoom(): Observable<any> {
    return this.http.get(`${this.editroom}`, httpJSONOptions);
  }

  repositoryList(): Observable<any> {
    return this.http.get(`${this.editsystem}`, httpOptions1);
  }
  getMainSchema(): Observable<any> {
    return this.http.get(`${this.schemaURL}`, httpOptions1);
  }

  getMainRoomSchema(): Observable<any> {
    return this.http.get(`${this.roomschemaURL}`, httpOptions1);
  }

  getMainSchema1(): Observable<any> {
    return this.http.get(`${this.schemaURL}`, httpOptions);
  }

  getSystemSchema(id: string): Observable<any> {
    return this.http.get(`${this.fetchAPI}${id}${this.schemaRepoURL}`, httpOptions);
  }

  getRoomSchema(id: string): Observable<any> {
    return this.http.get(`${this.fetchAPI}${id}${this.fetchRoomSchemaAPI}`, httpOptions);
  }

  roomRepositoryList(): Observable<any> {
    return this.http.get(`${this.editroom}`, httpOptions1);
  }

  createRoom(repoName: string, roomName: string): Observable<any> {

    const params1 = new HttpParams()
      .set('repository', repoName)
      .set('project_key', 'ROOM2')
      .set('description', roomName)
    return this.http.post(`${this.repoCreateURL}`, params1, httpOptions1);
  }

  commitfile(repoName: string, fileName: string, systemData: string): Observable<any> {

    const params = new HttpParams()
      .set('repository', repoName)
      .set('file_name', fileName)
      .set('json_data', systemData);

    return this.http.post(`${this.schema_systemURL}`, params, httpOptions1);
  }

  roomDeploy(rmName: string, repoName: string, systemName: string): Observable<any> {
    const params = new HttpParams()
      .set('Name', rmName)
      .set('ProgramInstanceId', repoName)
      .set('ProgramLibraryName', systemName)
      .set('AddressSetsLocation', 'true');
    return this.http.post(`${this.deployURL}`, params, httpOptions1);

  }


  createSystem(title: string, systemName: string): Observable<any> {

    const params1 = new HttpParams()
      .set('repository', title)
      .set('project_key', 'SYS2')
      .set('description', systemName)
    return this.http.post(`${this.repoCreateURL}`, params1, httpOptions1);
  }

  commitfile1(title: string, fileName: string, systemData: string): Observable<any> {

    const params = new HttpParams()
      .set('repository', title)
      .set('file_name', fileName)
      .set('json_data', systemData);


    return this.http.post(`${this.schema_systemURL}`, params, httpOptions1);
  }

  commitfile2(title: string, fileName: string, systemData: string): Observable<any> {

    const params = new HttpParams()
      .set('repository', title)
      .set('file_name', fileName)
      .set('json_data', systemData);

    return this.http.post(`${this.schema_systemURL}`, params, httpOptions1);
  }

  pipeLine(title: string): Observable<any> {

    const params1 = new HttpParams()
      .set('system_repo_name', title);

    return this.http.post(`${this.triggerPipeline}`, params1, httpOptions1);
  }

  updatepipeLine(title: string): Observable<any> {

    const params1 = new HttpParams()
      .set('system_repo_name', title);

    return this.http.put(`${this.updateRepoVariable}`, params1, httpOptions1);
  }

  getcommitFile(title: string, fileName: string, systemData: string): Observable<any> {

    const params = new HttpParams()
      .set('repository', title)
      .set('file_name', fileName)
      .set('json_data', systemData);


    return this.http.post(`${this.schema_systemURL}`, params, httpOptions1);
  }

  editpipeLine(title: string): Observable<any> {

    const params1 = new HttpParams()
      .set('system_repo_name', title);

    return this.http.post(`${this.edittriggerPipeline}`, params1, httpOptions1);
  }

  systemdeploy(title: string, systemName: string): Observable<any> {

    const params = new HttpParams()
      .set('FriendlyName', systemName)
      .set('Repository', title);

    return this.http.post(`${this.programLib}`, params, httpOptions1);
  }

  deleteRoom(repoName: string, roomName: string): Observable<any> {

    let deleteroom = environment.API + `/repository/?repository=` + repoName;
    return this.http.delete(`${deleteroom}`);
  }

  deleteRoomVC4(): Observable<any> {
    return this.http.get(environment.Orch_engine + `/driftrooms/`);
  }

  deleteSystemVC4(): Observable<any> {
    return this.http.get(environment.Orch_engine + `/driftsystems/`);
  }

  deleteSystem(repoName: string, systemName: string): Observable<any> {

    let deletesystem = environment.API + `/repository/?repository=` + systemName;
    return this.http.delete(`${deletesystem}`);
  }
  deleteSysRooms(repoName: string, systemName: string): Observable<any> {

    let deletesystem = environment.API + `deletesysrooms?system_repository=` + systemName;
    return this.http.delete(`${deletesystem}`);
  }

}
